<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class JsgridAsset extends AssetBundle
{
    // public $basePath = '@webroot';
    // public $baseUrl = '@web';
    public $sourcePath = '@common/custom-assets/jsGrid';

    public $css = [
        'css/jsgrid.css',
        'css/theme.css',
        'demos/demos.css',
        
    ];
    public $js = [
        'external/jquery/jquery-1.8.3.js',
        'demos/db.js',
        'dist/jsgrid.js',
        'dist/jsgrid.min.js',
        'src/jsgrid.core.js',
        'src/jsgrid.load-indicator.js',
        'src/jsgrid.load-strategies.js',
        'src/jsgrid.sort-strategies.js',
        'src/jsgrid.field.js',
        'src/fields/jsgrid.field.text.js',
        'src/fields/jsgrid.field.number.js',
        'src/fields/jsgrid.field.select.js',
        'src/fields/jsgrid.field.checkbox.js',
        'src/fields/jsgrid.field.control.js',               
    ];
    public $depends = [
        '\yii\web\JqueryAsset'
    ];
}
