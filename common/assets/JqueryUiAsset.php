<?php
namespace common\assets;

use yii\web\AssetBundle;

class JqueryUiAsset extends AssetBundle
{
    public $sourcePath = '@common/custom-assets/jquery-ui/';
    
    public $css = [
        'jquery-ui.structure.css',
        'themes/lefrog/jquery-ui.min.css',
        'themes/lefrog/jquery-ui.theme.min.css',
    ];
    
    public $js = [
        'jquery-ui.min.js',
    ];
   	
   	public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
