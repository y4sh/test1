<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * common application asset bundle.
 */
class JqGridAsset extends AssetBundle
{
    public $sourcePath = '@common/custom-assets/jqGrid';

    public $css = [
        'css/ui.jqgrid.css'
    ];
    public $js = [
        'js/i18n/grid.locale-en.js',
        'js/jquery.jqGrid.min.js',
        
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\JqueryUiAsset'
    ];
}
