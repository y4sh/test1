<?php

use yii\db\Migration;

class m190614_050806_create_cucet_result_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('cucet_result', [
            'id' => $this->primaryKey(),                       
            'program_id' => $this->integer()->notNull(), //Required Field
            'dept_id' => $this->integer()->notNull(), //Required Field
            'school_id' => $this->integer()->notNull(),  //Required Field
            'user_id' => $this->integer()->notNull() . ' DEFAULT 0',
            'full_name' => $this->string(),
            'gender' => $this->string(),
            'father_name' => $this->string(),
            'mother_name' => $this->string(),
            'dob' => $this->date()->notNull(), //Required Field
            'category' => $this->string(),
            'phy_handicap' => $this->string(),
            'handicap_type' => $this->string(),
            'landline_no' => $this->string(),
            'mobile_no' => $this->string()->notNull(), //Required Field
            'email_cucet' => $this->string()->notNull(), //Required Field
            'nationality' => $this->string(),
            'kashmiri_mig' => $this->string(),
            'defence_per' => $this->string(),
            'domicile_state' => $this->string(),
            'is_employed' => $this->string(),
            'qualifying_university' => $this->text(),
            'qualifying_year' => $this->string(),
            'qualifying_percentage' => $this->string(),
            'is_sponsered' => $this->string(),
            'gate_score' => $this->string(),
            'gate_percentile' => $this->string(),
            'gate_year' => $this->string(),
            'CSIR_JRF_Marks' => $this->string(),
            'CSIR_JRF_Year' => $this->string(),
            'present_address1' => $this->text(),
            'present_address2' => $this->text(),
            'district' => $this->string(),
            'present_state' => $this->string(),
            'present_pincode' => $this->string(),
            'cucet_application_no' => $this->string(),
            'religion' => $this->string(),
            'agency' => $this->string(),
            'marks_obtained' => $this->string(),
            'max_marks' => $this->string(),
            'grade' => $this->string(),
            'aadhaar_no' => $this->string(),
            'paid_appeared' => $this->string(),
            'i_course_code' => $this->string(),
            'v_course_name' => $this->string(),
            'i_un_code' => $this->string(),
            'v_test_paper_code' => $this->string(),
            'roll_no' => $this->string(),
            'total_marks' => $this->string(),
            'part_a' => $this->string(),
            'pat_b_descriptive' => $this->string(),
            'pat_b' => $this->string(),
            'part_b_chemistry' => $this->text(),
            'part_b_mathematics' => $this->text(),
            'part_b_physics' => $this->text(),
            'part_b_biology' => $this->text(),
            'part_b_economics' => $this->text(),
            'part_b_social_work' => $this->text(),
            'part_b_sociology' => $this->text(),
            'part_b_international_relations' => $this->text(),
            'part_b_med' => $this->text(),
            'part_b_political_science' => $this->text(),
            'part_b_education' => $this->text(),
            'part_c_language' => $this->text(),
            'part_c_life_sciences' => $this->text(),
            'part_c_phy_sci_math' => $this->text(),
            'part_c_social_science' => $this->text(),
            'remarks' => $this->text(), 
            'created_on' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%cucet_result}}');
    }
}
