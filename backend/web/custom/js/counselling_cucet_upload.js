$(document).ready(function() {
    $("select#program_id")
        .change(function() {
            var program_id = $("select#program_id").val() *1;

            if(program_id == 0) {
                $("#excel_uploaded_grid").jqGrid("clearGridData");
                alert("PLEASE SELECT PROGRAM");
            }
            else {
                var program = $("select#program_id option[value='"+program_id+"']").text();

                $("#excel_uploaded_grid")
                    .jqGrid(
                        "setGridParam", {
                            url:"get_cucet_uploaded_data",
                            postData: {
                                "program_id": program_id,
                                "_csrf-frontend": yii.getCsrfToken()
                            }
                        }
                    )
                    .jqGrid('setCaption', program + "'s UPLOADED DATA FOR THIS YEAR PROGRAM")
                    .trigger("reloadGrid");
            }
        });    

	$('form#upload-excel-form').on('beforeSubmit', function(e) {
		var program_id = $("#program_id").val() * 1;
		if(program_id == 0) {
			alert("PLEASE SELECT PROGRAM FROM THE LIST PLEASE!!!");
			return false;
		}
		
        if(confirm("ARE YOU SURE YOU WANT TO UPLOAD SELECTED DATA?")) {
            var form = $(this);
            var formData = new FormData($(form)[0]);
            formData.append('program_id', program_id);//you can append it to formdata with a proper parameter name 

            $.ajax({
                url: form.attr('action'),  //Server script to process data
                type: form.attr('method'),
                data: formData, // Form data
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if(response=="data-updated") {
                        $("form#upload-excel-form").trigger('reset');
                        $("select#program_id").val(program_id);
                        $("#excel_uploaded_grid")
                            .jqGrid(
                                "setGridParam", {
                                    url:"get_cucet_uploaded_data",
                                    postData: {
                                        "program_id": program_id,
                                        "_csrf-frontend": yii.getCsrfToken()
                                    }
                                }
                            )
                            // .jqGrid('setCaption', program + "'s UPLOADED DATA FOR THIS YEAR PROGRAM")
                            .trigger("reloadGrid");

                        alert("File Data uploaded to server !!!");
                    }
                    if(response=="saving-error") alert("Error while uploading data !!!");
                    if(response=="unauth") alert("Unauthorize Access !!!");
                },
                error: function() {
                    alert('ERROR at server side. Contact Your administrator!!');
                }
            });
        }
        else alert("ACTIONED CANCLLED!!!");		
 	})
	.on('submit', function(e) {
	    e.preventDefault();
	});

    $("#excel_uploaded_grid").jqGrid({
        // url: 'get_cucet_uploaded_data',            
        datatype: "json",
        mtype: "POST",
        colNames:["Application No", "FULL", "FATHER", "MOTHER", "MOBILE", "EMAIL", "TOTAL"],
        colModel: [
            {index : 'cucet_application_no', name: 'cucet_application_no', width: 80, align:"center"},
            {index : 'full_name', name: 'full_name', width: 200, align:"left"},
            {index: 'father_name', name: 'father_name', width: 200, align:"left"},
            {index: 'mother_name', name: 'mother_name', width: 200, align:"left"},
            {index: 'mobile_no', name: 'mobile_no', width: 70, align:"center"},
            {index: 'email_cucet', name: 'email_cucet', width: 250, align:"left"},
            {index: 'total_marks', name: 'total_marks', width: 60, align:"right"}
        ],      
        sortname: 'cucet_application_no',
        sortorder : 'ASC',
        viewrecords: true,
        widh: "100%",
        height: "100%",
        rowNum: 1000,
        caption: "UPLOADED DATA FROM CUCET RESULT",
        pager: "#pager_excel_uploaded_grid"
    })
    .navGrid('#pager_excel_uploaded_grid',
        // the buttons to appear on the toolbar of the grid
        { edit: false, add: false, del: false, search: false, refresh: false, view: false},
        // options for the Edit Dialog
        {
            /*editCaption: "The Edit Dialog",
            recreateForm: true,
            url: "save_inst",
            //checkOnUpdate : true,
            //checkOnSubmit : true,
            beforeSubmit : function( postdata, form , oper) {
                if( confirm('Are you sure you want to update this row?') ) {
                    // do something
                    return [true,''];
                } else {
                    return [false, 'You can not submit!'];
                }
            },
            closeAfterEdit: true,
            reloadAfterSubmit : true,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }*/
        },
        // options for the Add Dialog
        {
            /*closeAfterAdd: true,
            recreateForm: true,
            url: "save_inst",
            addCaption: "Add Inst",
            bottominfo: "Fields marked with (*) are required",
            reloadAfterSubmit : true,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }*/
        },
        // options for the Delete Dailog
        {
          /*  url: "save_inst",
            reloadAfterSubmit : true,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }*/
        }
    )
    .navButtonAdd('#pager_excel_uploaded_grid',
        {
            buttonicon: "ui-icon-circle-arrow-s",
            title: "Download",
            caption: "Downlaod",
            position: "first",
            onClickButton: function () {
                var grid = $("#excel_uploaded_grid");
                var rowKey = grid.jqGrid('getGridParam',"selrow");
                if(rowKey == null) {
                    alert("PLEASE SELECT APPLICANT TO DOWNLOAD THE FORM!!!")
                }
                // alert(rowKey);
                var jqxhr = $.post( "get_student_details", { "_csrf-backend": yii.getCsrfToken(), "rowKey": rowKey},
                    function(data) {
                        $("<a>").attr("href", '/export/MyFile.pdf').attr("target", "_blank")[0].click();
                        // alert(data)                                        ;
                });
            }
        }
    );
})
.ajaxStart(function() {
    $("#overlay-loading, #loading-image").show();
})
.ajaxStop(function() {
    $("#overlay-loading, #loading-image").hide();
});
