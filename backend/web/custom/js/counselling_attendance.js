$(document).ready(function() {
	$("#application_no").autocomplete({
	    source: function( request, response ) {
	    	var program_id = $('#program_id').val() *1;
	    	if(program_id == 0) {
	    		alert("PLEASE SELECT PROGRAM FROM THE LIST FIRST!!!");
	    		return false;
	    	}
	     	$.ajax({
		        url: "get_application_no_autocomplete",
		        type: "POST",
		        dataType: "json",
		        data: {
		        	application_no: request.term,
		        	program_id:program_id,
		        	"_csrf-backend": yii.getCsrfToken()
		        },
		        success: function(data) {
		          	response(data);
		        },
	      	});
	    },
	    minLength: 4,
        select: function( event, ui ) {
    	    $("#uploaded_id").val(ui.item.id);
	    	var program_id = $('#program_id').val() *1;
	    	var uploaded_id = $('#uploaded_id').val() *1;

            $("#attendance")
                .jqGrid(
                    "setGridParam", {
                        url:"get_cucet_attendance_data",
                        postData: {
                            "program_id": program_id,
                            "uploaded_id": uploaded_id,
                            "_csrf-backend": yii.getCsrfToken()
                        }
                    }
                )
                .trigger("reloadGrid");

        },
        create: function ( event, ui ) {
    	    $("#application_no").val("");
        },
        change: function( event, ui ) {
        	if(!ui.item) {
        	    //http://api.jqueryui.com/autocomplete/#event-change -
        	    // The item selected from the menu, if any. Otherwise the property is null
        	    //so clear the item for force selection
        	    $("#application_no").val("");
        	}
		}
	});

    $("#attendance").jqGrid({
        datatype: "json",
        mtype: "POST",
        colNames:["APPLICATION NO", "FULL NAME", "FATHER", "MOTHER", "MOBILE", "EMAIL", "TOTAL", "ATTENDANCE"],
        colModel: [
            {index : 'cucet_application_no', name: 'cucet_application_no', width: 120, align:"center"},
            {index : 'full_name', name: 'full_name', width: 180, align:"left"},
            {index: 'father_name', name: 'father_name', width: 180, align:"left"},
            {index: 'mother_name', name: 'mother_name', width: 180, align:"left"},
            {index: 'mobile_no', name: 'mobile_no', width: 70, align:"center"},
            {index: 'email_cucet', name: 'email_cucet', width: 180, align:"left"},
            {index: 'total_marks', name: 'total_marks', width: 60, align:"right"},
            {
            	index: 'attendance', name: 'attendance', width: 90, align:"center",
			    /*cellattr: function (rowId, val, rawObject, cm, rdata) {
			    	if (val == "PRESENT") return 'class="positive-row"';
			    	else return 'class="negative-row"';
			    }*/
            }
        ],      
        sortname: 'cucet_application_no',
        sortorder : 'ASC',
        viewrecords: true,
        widh: "100%",
        height: "100%",
        rowNum: 1000,
        caption: "MARK ATTENDANCE FOR...",
        pager: "#pager_attendance",
        rowattr: function (rd) {
			var attendance = rd.attendance
		    if (attendance == "ABSENT") return {"class": "negative-row"}
		    if (attendance == "PRESENT") return {"class": "positive-row"}
		},

    })
    .navGrid('#pager_attendance', { edit: false, add: false, del: false, search: false, refresh: false, view: false}, {}, {}, {})
	.navButtonAdd('#pager_attendance', {
		caption:"<span class='glyphicon glyphicon-ok-circle'></span> Mark Applicant Present!!!", 
		buttonicon:"none", 
		onClickButton: function() {
			var program_id = $("#program_id").val() *1, uploaded_id = $("#uploaded_id").val() *1;
			var attendance_id = $("#attendance").jqGrid("getGridParam", "selrow");

			if(program_id == 0) {
	    		alert("PLEASE SELECT PROGRAM FROM THE LIST FIRST!!!");
				return false;
			}

			if(uploaded_id == 0) {
				alert("PLEASE ENTER APPLICATION NUMBER !!!");
				return false;
			}

			if(attendance_id == 0) {
				alert("PLEASE SELECT STUDENT TO MARK HIS PRESENT!!!");
				return false;
			}
			var r = confirm("ARE YOU SURE YOU WANT TO MARK THIS APPLICANT AS PRESENT!");
			if (r == true) {
				$.post("save_cucet_attendance_data", {"oper":"edit", program_id:program_id, uploaded_id:uploaded_id, attendance_id:attendance_id} , function(data) {
					if(data == "data-updated") {
			            $("#attendance")
			                .jqGrid(
			                    "setGridParam", {
			                        url:"get_cucet_attendance_data",
			                        postData: {
			                            "program_id": program_id,
			                            "uploaded_id": uploaded_id,
			                            "_csrf-backend": yii.getCsrfToken()
			                        }
			                    }
			                )
			                .trigger("reloadGrid");

						alert("Data Added!!!");
					}
					else alert("Data failed to add!!!");
				});	
			}
			else alert("ACTIONED CANCLLED!!!");
		}, 
		position:"last"
	})    
})
.ajaxStart(function() {
    $("#overlay-loading, #loading-image").show();
})
.ajaxStop(function() {
    $("#overlay-loading, #loading-image").hide();
});
