$(document).ready(function() {
    var a = $("#category").jqGrid({
        mtype: "POST",
        height: "100%",
        postData: {
            "_csrf-frontend": yii.getCsrfToken()
        },
        url: "http://cas-backend.test/adm/default/export",
        datatype: "json",
        colNames: [ "Category" ],
        colModel: [ {
            name: "category",
            index: "category",
            width: 380,
            editable: true,
            edittype: "textarea",
            editrules: {
                required: true
            }
        } ],
        pager: "#pager_category",
        sortname: "category",
        sortorder: "ASC",
        rowNum: 20,
        viewrecords: true,
        caption: "Category"
    }).navGrid("#pager_category", {
        edit: true,
        add: true,
        del: false,
        search: false,
        refresh: false
    }, 
    {
        url: "save_category",
        closeAfterEdit: true,
        editCaption: "Edit Category",
        bottominfo: "Fields marked with (*) are required"
    },
    {
        url: "save_category",
        closeAfterAdd: true,
        addCaption: "Add Category",
        bottominfo: "Fields marked with (*) are required"
    }, 
    {
        url: "save_category",
        closeAfterDel: true
    });
});