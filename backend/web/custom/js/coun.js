$(document).ready(function () {
    $("#jqGrid").jqGrid({
        url: 'get_cucet',            
        datatype: "json",
        mtype: "POST",
        colModel: [ 
            
            {
                label : 'ID',
                name: 'id',
                width: 100,
                key: true,                              
            }, 
            {
                label : 'Name',
                name: 'full_name',
                width: 100,                
                editable: true,
                editrules : { required: true}
            },                       
            {
                label: 'Father',
                name: 'father_name',
                width: 140,
                editable: true ,
                editrules : { required: true}
            },                       
            {
                label: 'Mother',
                name: 'mother_name',
                width: 140,
                editable: true ,
                editrules : { required: true}
            },                       
            {
                label: 'Mobile',
                name: 'mobile_no',
                width: 140,
                editable: true ,
                editrules : { required: true}
            },                       
            {
                label: 'Email',
                name: 'email_cucet',
                width: 140,
                editable: true ,
                editrules : { required: true}
            },
            {
                label: 'Total Marks',
                name: 'total_marks',
                width: 140,
                editable: true ,
                editrules : { required: true}
            },
            
        ],      
        sortname: 'instructerid',
        sortorder : 'asc',
        loadonce: true,
        viewrecords: true,
        widh: 780,
        height: 200,
        rowNum: 1000,
        pager: "#jqGridPager"
    });

    $('#jqGrid').navGrid('#jqGridPager',
        // the buttons to appear on the toolbar of the grid
        { edit: true, add: true, del: true, search: false, refresh: false, view: false, position: "left", cloneToTop: false },
        // options for the Edit Dialog
        {
            editCaption: "The Edit Dialog",
            recreateForm: true,
            url: "save_inst",
            //checkOnUpdate : true,
            //checkOnSubmit : true,
            beforeSubmit : function( postdata, form , oper) {
                if( confirm('Are you sure you want to update this row?') ) {
                    // do something
                    return [true,''];
                } else {
                    return [false, 'You can not submit!'];
                }
            },
            closeAfterEdit: true,
            reloadAfterSubmit : true, 
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            closeAfterAdd: true,
            recreateForm: true,
            url: "save_inst", 
            addCaption: "Add Inst",
            bottominfo: "Fields marked with (*) are required",
            reloadAfterSubmit : true, 
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            url: "save_inst",
            reloadAfterSubmit : true, 
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        }
    );
});


