$(document).ready(function() {

    $("select#programId")
        .change(function() {
            var programId = $("select#programId").val() *1;

            if(programId == 0) {
                $("#batch, #list").jqGrid("clearGridData");
                alert("PLEASE SELECT PROGRAM");
            }
            else {
                var program = $("select#programId option[value='"+programId+"']").text();

                // $("#batch_students").jqGrid("clearGridData").jqGrid('setCaption', "Please select batch first.");
                $("#batch")
                    .jqGrid(
                        "setGridParam", {
                            // url:"get_batch",
                            postData: {
                                "programId": programId,
                                "_csrf-frontend": yii.getCsrfToken()
                            }
                        }
                    )
                    .jqGrid('setCaption', program + "'s Batchs")
                    .trigger("reloadGrid");

                $("#general")
                    .jqGrid(
                        "setGridParam", {
                            url:"get_merit_general",
                            postData: {
                                "program_id": programId,
                                "_csrf-backend": yii.getCsrfToken()
                            }
                        }
                    )
                    // .jqGrid('setCaption', program + "'s Batchs")
                    .trigger("reloadGrid");
                $("#st")
                    .jqGrid(
                        "setGridParam", {
                            url:"get_merit_st",
                            postData: {
                                "program_id": programId,
                                "_csrf-backend": yii.getCsrfToken()
                            }
                        }
                    )
                    // .jqGrid('setCaption', program + "'s Batchs")
                    .trigger("reloadGrid");
                $("#sc")
                    .jqGrid(
                        "setGridParam", {
                            url:"get_merit_sc",
                            postData: {
                                "program_id": programId,
                                "_csrf-backend": yii.getCsrfToken()
                            }
                        }
                    )
                    // .jqGrid('setCaption', program + "'s Batchs")
                    .trigger("reloadGrid");
            }
        });

    /***************
    ** BATCH Grid **
    ****************/
    var batch = $("#batch").jqGrid({
        mtype: "POST",
        height: "100%",
        width: "100%",
        postData: {"_csrf-frontend": yii.getCsrfToken(), "programId":0},
        url:"/adm/batch/get_batch",
        datatype: "json",
        colNames:[
            "Index", "Start", "End", "Intake", "GEN", "Cream", "Non Creamy","ST",
            "SC","PWD", "KM", "FF","EX S Men", "programId", "deptId", "schoolId",
            "Full Count", "Lock", "Current Semester"
        ],
        colModel:[
            {name:"batchIndex", index:"batchIndex", width:90, editable:false, align:"center"},
            {name:"start", index:"start", width:60, editable:true, editrules:{required:true}, align:"center"},
            {name:"end", index:"end", width:60, editable:true, editrules:{required:true}, align:"center"},
            {name:"intake", index:"intake", width:50, editable:false, editrules:{required:true}, align:"center"},
            {name:"general", index:"general", width:50, editable:false, editrules:{required:true}, align:"center"},
            {name:"obc_creamy", index:"obc_creamy", width:80, editable:false, editrules:{required:true}, align:"center"},
            {name:"obc_non_creamy", index:"obc_non_creamy", width:80, editable:false, editrules:{required:true}, align:"center"},
            {name:"st", index:"st", width:30, editable:false, editrules:{required:true}, align:"center"},
            {name:"sc", index:"sc", width:30, editable:false, editrules:{required:true}, align:"center"},
            {name:"pwd", index:"pwd", width:40, editable:false, editrules:{required:true}, align:"center", hidden:true},
            {name:"km", index:"km", width:30, editable:false, editrules:{required:true}, align:"center", hidden:true},
            {name:"ff", index:"ff", width:30, editable:false, editrules:{required:true}, align:"center", hidden:true},
            {name:"xs_men", index:"xs_men", width:70, editable:false, editrules:{required:true}, align:"center", hidden:true},
            {name:"programId", index:"programId", width:1, hidden:true},
            {name:"deptId", index:"deptId", width:1, hidden:true},
            {name:"schoolId", index:"schoolId", width:1, hidden:true},
            {name:"fullCount", index:"fullCount", width:80, editable:false,hidden:true},
            {
                name:"lock", index:"lock", width:40, hidden:true, editable:false, align:"center",
                editoptions: { value: "0:1" }, formatter: "checkbox"
            },
            {
                name:"current_semester", index:"current_semester", width:80, hidden:false, editable:true, align:"center",
                edittype:'select', editoptions:{value:{1:1,2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9, 10:10}}, hidden:true
            }
        ],
        pager: "#pager_batch",
        sortname: "start",
        sortorder: "DESC",
        viewrecords: true,
        caption: "CUSB PROGRAM BATCH",
        rownumbers: false,
        rowNum: 10,
        /*onSelectRow: function(id) {
            var batchIndex = batch.jqGrid("getRowData", id).batchIndex;

            batchStudentGird
                .jqGrid("clearGridData")
                .jqGrid(
                    "setGridParam", {
                        url:"get_batch_students",
                        postData: {
                            "batchId": id,
                            'batchIndex' : batchIndex
                        }
                    }
                )
                .jqGrid('setCaption', batchIndex + "'s Batch Students")
                .trigger("reloadGrid");
                $("#list").jqGrid("clearGridData");
        },*/
    })
    .jqGrid('setGroupHeaders', {
      useColSpanStyle: false, 
      groupHeaders:[
        {startColumnName: 'batchIndex', numberOfColumns: 3, titleText: '<center><em>Batch</em></center>'},
        {startColumnName: 'general', numberOfColumns: 9, titleText: '<center><em>Quota</em></center>'}
      ]
    })
    .navGrid('#pager_batch',
        {
            edit:false,
            add:false, 
            addfunc: function() {
                /*var programId = $("select#programId").val() *1;
                var deptId = $("select#deptId").val() *1;
                var schoolId = $("select#schoolId").val() *1;

                if(schoolId == 0) {             
                    $("#batch").jqGrid("clearGridData"); 

                    alert("PLEASE SELECT SCHOOL.");
                    return false;
                }
                else if(deptId == 0) {              
                    $("#batch").jqGrid("clearGridData"); 

                    alert("PLEASE SELECT DEPARTMENT.");
                    return false;
                }
                else if(programId == 0) {               
                    $("#batch").jqGrid("clearGridData"); 

                    alert("PLEASE SELECT PROGRAM.");
                    return false;
                }
                else {
                    $( "input#start" ).datepicker();

                    batch.jqGrid('editGridRow', "new", {
                        reloadAfterSubmit:true,
                        url: "save_batch",
                        closeAfterAdd: true,
                        beforeShowForm: function(form) { $('#tr_current_semester', form).hide(); },
                        editData: {
                            "schoolId":schoolId,
                            "deptId":deptId,
                            "programId":programId,
                            "_csrf-frontend":yii.getCsrfToken()
                        },
                        afterShowForm:function() {
                            $("input#start").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showOn: "button",
                                buttonImage: "../../custom/images/calendar.gif",
                                buttonImageOnly: true,
                                dateFormat: "d M y"
                            });

                            $("input#end").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showOn: "button",
                                buttonImage: "../../custom/images/calendar.gif",
                                buttonImageOnly: true,
                                dateFormat: "d M y"
                            });
                        },
                    });
                }*/
            },
            del:false, delfunc: function() {
                // alert("DELETE FUNCTIONALITY IS NOT ALLOWED");
            },
            search:false, refresh:false
        },
        //Edit Options
        {
            /*"url": "save_batch", closeAfterEdit: true,
            beforeShowForm: function(form) { $('#tr_current_semester', form).show(); },
            afterShowForm:function() {
                $("input#start").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showOn: "button",
                    buttonImage: "../../custom/images/calendar.gif",
                    buttonImageOnly: true,
                    dateFormat: "d M y"
                });

                $("input#end").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showOn: "button",
                    buttonImage: "../../custom/images/calendar.gif",
                    buttonImageOnly: true,
                    dateFormat: "d M y"
                });
            }*/
        },
        //Add Options
        {/*"url": "save_batch", closeAfterAdd: true*/},
        //Del Option
        {/*"url": "save_batch", closeAfterDel: true*/}
    )
    /*.navButtonAdd('#pager_batch', {
        caption:"<span class='glyphicon glyphicon-list-alt'></span> Populate Courses", 
        buttonicon:"none", 
        onClickButton: function() {
            var programId = $("select#programId").val() *1;
            var batchId = $(this).jqGrid("getGridParam", "selrow");
            if(programId == 0) {
                alert("PLEASE SELECT PROGRAM FIRST TO POPULATE COURSES");
                return false;
            }
            if(batchId == null) {
                alert("PLEASE SELECT BATCH FIRST TO POPULATE COURSES LIST");
                return false;
            }
            $.post( "get_courses_list", {programId: programId, batchId:batchId}, function( data ) {
                $("#admin-courses-list").empty().append(data);
                $("#myPopulateCoursesModal").modal('show');
            });
        }, 
        position:"last"
    });*/

    var generalGrid = $("#general").jqGrid({
        mtype: "POST",
        height: "100%",
        postData: {"_csrf-frontend": yii.getCsrfToken()},
        // url:"get_merit_general",
        datatype: "json",
        colNames:["Name", "Code", "Score"],
        colModel:[
            {name:"name", index:"name", width:200, sortable:false },
            {name:"code", index:"code", width:70, sortable:false},
            {index: 'total_marks', name: 'total_marks', width: 60, align:"right", sortable:false}
        ],
        pager: "#pager_general",
        sortname: "total_marks",
        sortorder: "DESC",
        rowNum: 100,
        viewrecords: true,
        caption: "GENERAL MERIT LIST",
        rownumbers: false,
    })
    .navGrid('#pager_general',
        {
            edit:false, add:false, del:false, search:false, refresh:false
        },
        //Edit Options
        {
        },
        //Add Options
        {
        },
        //Del Option
        {
        }
    )
    .navButtonAdd('#pager_general',
        {
            buttonicon: "ui-icon-circle-arrow-s",
            title: "Download",
            caption: "Downlaod",
            position: "first",
            onClickButton: function () {
                var grid = $("#general");
                var rowKey = grid.jqGrid('getGridParam',"selrow");
                alert(rowKey);

                 var jqxhr = $.post( "get_student_details", { "_csrf-backend": yii.getCsrfToken(), "rowKey": rowKey},
                        function(data) {
                            // $("<a>").attr("href", data).attr("target", "_blank")[0].click();
                            alert(data.url)                      
                            
                    });  


            }
        }
    );
     


    var stGrid = $("#st").jqGrid({
        mtype: "POST",
        height: "100%",
        postData: {"_csrf-backend": yii.getCsrfToken()},
        // url:"get_merit_sc",
        datatype: "json",
        colNames:["Name", "Code", "Score"],
        colModel:[
            {name:"name", index:"name", width:200, sortable:false},
            {name:"code", index:"code", width:70, sortable:false},
            {index: 'total_marks', name: 'total_marks', width: 60, align:"right", sortable:false}
        ],
        pager: "#pager_st",
        sortname: "total_marks",
        sortorder: "DESC",
        rowNum: 100,
        viewrecords: true,
        caption: "ST MERIT LIST",
        rownumbers: false,
    })
    .navGrid('#pager_sc',
        {
            edit:false, add:false, del:false, search:false, refresh:false
        },
        //Edit Options
        {
        },
        //Add Options
        {
        },
        //Del Option
        {
        }
    );
    var scGrid = $("#sc").jqGrid({
        mtype: "POST",
        height: "100%",
        postData: {"_csrf-backend": yii.getCsrfToken()},
        // url:"get_merit_sc",
        datatype: "json",
        colNames:["Name", "Code", "Score"],
        colModel:[
            {name:"name", index:"name", width:200, sortable:false},
            {name:"code", index:"code", width:70, sortable:false},
            {index: 'total_marks', name: 'total_marks', width: 60, align:"right", sortable:false}
        ],
        pager: "#pager_sc",
        sortname: "total_marks",
        sortorder: "DESC",
        rowNum: 100,
        viewrecords: true,
        caption: "SC MERIT LIST",
        rownumbers: false,
    })
    .navGrid('#pager_sc',
        {
            edit:false, add:false, del:false, search:false, refresh:false
        },
        //Edit Options
        {
        },
        //Add Options
        {
        },
        //Del Option
        {
        }
    );


})
.ajaxStart(function() {
    $("#overlay-loading, #loading-image").show();
})
.ajaxStop(function() {
    $("#overlay-loading, #loading-image").hide();
});
