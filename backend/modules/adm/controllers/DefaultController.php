<?php

namespace app\modules\adm\controllers;
use Yii;
use yii\web\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use yii\himiklab\jqgrid\actions\JqGridActiveAction;
use app\modules\adm\models\Studentlogin;
use app\modules\adm\models\Subjects2;
$page=new Studentlogin();

/**
 * Default controller for the `adm` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    /*public function actions()
    {
        return [
            'jqgrid' => [
                'class' => JqGridActiveAction::className(),
                'model' => Studentlogin::className(),
            ],
        ];
    }*/
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionCurd()
    {
        return $this->render('curd');
    }
    public function actionDownload()
    {   
		

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');
		$writer = new Xlsx($spreadsheet);
		$writer->save('/export/hello_world.xlsx');
    }
    public function actionFeed(){
        $query = (new \yii\db\Query())
                    ->select("*")
                    ->from(['feedback_data'])                    
                    ->all();  
        return $this->render('feed',['result'=>$query]);                
        ///home/yash/http/yii2-cas/backend/modules/adm/views/default/index.php

    }
    public function actionExport(){
         $query = (new \yii\db\Query())
                    ->select("*")
                    ->from(['subjects2'])                    
                    ->all();  
        return $this->asJson($query); 
    }
    public function actionSave_subject(){
        $subjects2  =   new Subjects2();
        $coursename = $_POST['coursename'];
        print $coursename;
                
      /* if ($subjects2->load(Yii::$app->request->post())) {
        $coursename = $_POST['coursename'];
        print $coursename;
       }
       else return "errorpop";*/
    }
    
}
