<?php
$this->title = 'Your Courses';

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\assets\JqGridAsset;

$this->registerJsFile('@web/js/curd.js', ['depends' => [JqGridAsset::className()]]);
?>

<div class="container">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>   
</div>

