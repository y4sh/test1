<?php
    use yii\web\View;
    use common\assets\JqGridAsset;


    $this->registerJsFile('@web/js/jqgrid.js', ['depends' => [JqGridAsset::className()]]);
?>
    <div class="col-xs-9 col-sm-5 col-md-5">
        <table id="category"></table>
        <div id="pager_category"></div>
        <br>
    </div>