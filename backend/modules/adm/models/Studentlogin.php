<?php

namespace app\modules\adm\models;

use Yii;

/**
 * This is the model class for table "studentlogin".
 *
 * @property int $loginid
 * @property string $loginname
 * @property string $loginpassword
 * @property string $campusname
 * @property string $coursename
 * @property string $sessionname
 * @property string $semestername
 * @property int $subjects
 * @property int $submited_subjects
 */
class Studentlogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'studentlogin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['loginname', 'loginpassword', 'campusname', 'coursename', 'sessionname', 'semestername'], 'required'],
            [['subjects', 'submited_subjects'], 'integer'],
            [['loginname', 'loginpassword', 'campusname', 'coursename', 'sessionname', 'semestername'], 'string', 'max' => 255],
            [['loginname'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'loginid' => 'Loginid',
            'loginname' => 'Loginname',
            'loginpassword' => 'Loginpassword',
            'campusname' => 'Campusname',
            'coursename' => 'Coursename',
            'sessionname' => 'Sessionname',
            'semestername' => 'Semestername',
            'subjects' => 'Subjects',
            'submited_subjects' => 'Submited Subjects',
            
        ];
    }
}
