<?php

namespace app\modules\adm\models;

use Yii;

/**
 * This is the model class for table "subjects2".
 *
 * @property int $subjectid
 * @property string $coursename
 * @property string $sessionname
 * @property string $semestername
 * @property string $subjectname
 * @property string $subjectcode
 * @property string $ins_dept
 * @property string $instructername
 */
class Subjects2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subjects2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coursename', 'sessionname', 'semestername', 'instructername'], 'string', 'max' => 255],
            [['subjectname'], 'string', 'max' => 256],
            [['subjectcode'], 'string', 'max' => 10],
            [['ins_dept'], 'string', 'max' => 100],
            [['subjectname', 'subjectcode'], 'unique', 'targetAttribute' => ['subjectname', 'subjectcode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subjectid' => 'Subjectid',
            'coursename' => 'Coursename',
            'sessionname' => 'Sessionname',
            'semestername' => 'Semestername',
            'subjectname' => 'Subjectname',
            'subjectcode' => 'Subjectcode',
            'ins_dept' => 'Ins Dept',
            'instructername' => 'Instructername',
        ];
    }
}
