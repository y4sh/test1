<?php
namespace app\modules\counselling\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadExcelData extends Model
{
    /**
     * @var excel_file
     */
    public $excel_file;

    public function rules()
    {
        return [
            [['excel_file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }
    
   /* public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }*/
}