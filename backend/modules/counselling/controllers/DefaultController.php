<?php

namespace app\modules\counselling\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\HtmlPurifier;
use yii\filters\AccessControl;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Mpdf\Mpdf;
use app\modules\counselling\models\UploadExcelData;
/**
 * Default controller for the `counselling` module
 */
class DefaultController extends Controller
{

	/**
	 * @inheritdoc
     * Access control function for restricting only logged in user to have access
	 */
	public function behaviors()
	{
	    return [
	        'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [
	                [
	                    'actions' => [
	                        'index', 'upload_excel_sheet', 'get_cucet_uploaded_data','download_pdf','pdfl','get_student_details',
	                        'attendance',
	                        'get_application_no_autocomplete', 'get_cucet_attendance_data', 'save_cucet_attendance_data',
	                        'merit',
	                        'get_merit_general', 'get_merit_st', 'get_merit_sc', 'sample_pdf'
	                    ],
	                    'allow' => true,
	                    'roles' => ["@"]
	                ],
	            ]
	        ],
	    ];
	}

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionPdfl(){
       return  $this->render(pdfl);
    }

    public function actionIndex()
    {
    	/**
    	* Getting school, dept and program data to show it in dropdown.
    	* This data is controller from Admin > School Tab.
    	*
    	*/
    	$program = Yii::$app->db->createCommand(
            "SELECT ap.id AS pid, asch.name AS sname, ad.name AS dname, ap.name AS pname
            FROM admin_schools AS asch
            LEFT JOIN admin_dept AS ad ON ad.schoolId=asch.id JOIN admin_program AS ap ON ap.deptId=ad.id
            WHERE ap.duration > 0
            ORDER BY pname, dname, sname"
        )
        ->queryAll();
        $programOpt = '<option role="option" value="0" selected>Choose From Schools > Center > Program To See Batched Created For Program.</option>';

    	foreach ($program as $sc) $programOpt .= "<option role='option' value='" . $sc['pid'] . "'>" . $sc['sname'] . " > " . $sc['dname'] . " > " . $sc['pname'] ."</option>";

    	$UploadExcelData = new UploadExcelData();

        return $this->render('index', [
        	'programOpt' => $programOpt,
        	'UploadExcelData' => $UploadExcelData
        ]);
    }

    public function actionUpload_excel_sheet()
    {
		$UploadExcelData = new UploadExcelData();

        if (Yii::$app->request->isPost) {
            $UploadExcelData->excel_file = UploadedFile::getInstance($UploadExcelData, 'excel_file');
            $program_id = $_POST['program_id'];

            if(empty($program_id *1)) {
	            \Yii::$app->session->setFlash('danger', "Please select program from dropdown list please.");
	            \Yii::$app->response->redirect(['/counselling/default/index']); //redirect user to student page
	            \Yii::$app->end();
	        }
	        // else return true;

            $dept_id = Yii::$app->db->createCommand("SELECT deptId FROM admin_program WHERE id=:program_id")
			    ->bindValue(":program_id", $program_id)
			    ->queryScalar();

	        $school_id = Yii::$app->db->createCommand("SELECT schoolId FROM admin_dept WHERE id=:dept_id")
	            ->bindValue(":dept_id", $dept_id)
	            ->queryScalar();

            if ($UploadExcelData->excel_file && $UploadExcelData->validate()) {                
                $spreadsheet = new Spreadsheet();

    			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xls");  
				$spreadsheet = $reader->load($UploadExcelData->excel_file->tempName);
				
				$rows = $spreadsheet->getActiveSheet()->toArray();
				$final_array = array();
				$f_row = array_shift($rows);//finding first row of sheet	

				foreach ($rows as $key => $value) {
					# code...
					array_unshift($value, $school_id);
					array_unshift($value, $dept_id);
					array_unshift($value, $program_id);
					$final_array[] = $value;
				}

				$connection = Yii::$app->db;
				$transaction = $connection->beginTransaction();
				try {
					    $connection->createCommand()->batchInsert('cucet_result', [
					            'program_id', //Required Field ****
					            'dept_id', //Required Field ******
					            'school_id',  //Required Field *****
					            'full_name',
					            'gender',
					            'father_name',
					            'mother_name',
					            'dob', //Required Field
					            'category',
					            'phy_handicap',
					            'handicap_type',
					            'landline_no',
					            'mobile_no',
					            'email_cucet',
					            'nationality',
					            'kashmiri_mig',
					            'defence_per',
					            'domicile_state',
					            'is_employed',
					            'qualifying_university',
					            'qualifying_year',
					            'qualifying_percentage',
					            'is_sponsered',
					            'gate_score',
					            'gate_percentile',
					            'gate_year',
					            'CSIR_JRF_Marks',
					            'CSIR_JRF_Year',
					            'present_address1',
					            'present_address2',

					            'district',
					            'present_state',
					            'present_pincode',
					            'cucet_application_no',
					            'religion',
					            'agency',
					            'marks_obtained',
					            'max_marks',
					            'grade',
					            'aadhaar_no',
					            'paid_appeared',
					            'i_course_code',
					            'v_course_name',
					            'i_un_code',
					            'v_test_paper_code',
					            'roll_no',
					            'total_marks',
					            'part_a',
					            'pat_b_descriptive',
					            'pat_b',
					            'part_b_chemistry',
					            'part_b_mathematics',
					            'part_b_physics',
					            'part_b_biology',
					            'part_b_economics',
					            'part_b_social_work',
					            'part_b_sociology',
					            'part_b_international_relations',
					            'part_b_med',
					            'part_b_political_science',

					            'part_b_education',
					            'part_c_language',
					            'part_c_life_sciences',
					            'part_c_phy_sci_math',
					            'part_c_social_science',
					            'remarks'
						    	],
						    	$final_array
					    	)	
					    	->execute();		
		    		//.... other SQL executions
		    	    $transaction->commit();
		    	    return "data-updated";
		    	}
		    	catch (\Exception $e) {
		    	    $transaction->rollBack();
		    	    throw $e;
		    	    return "saving-error";
		    	}
		    	catch (\Throwable $e) {
		    	    $transaction->rollBack();
		    	    throw $e;
		    	    return "saving-error";
		    	}					    			    
        	}
        	else return "File Post Error!";
        }
    }

    public function actionGet_cucet_uploaded_data()
    {
        $program_id = $_POST['program_id'] *1;
        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);
            
    	if(!$sidx) $sidx = "name";
        if(!$sord) $sord = "ASC";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cucet_result WHERE program_id=:program_id")->bindParam(":program_id", $program_id)->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
 
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE program_id=:program_id ORDER BY $sidx $sord LIMIT :st, :lt")
        	->bindParam(":program_id", $program_id)
            ->bindValue(":st", $start *1)
            ->bindValue(":lt", $limit *1)
            ->queryAll();

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];
            $responce->rows[$i]['cell'] = array($row['cucet_application_no'], $row['full_name'], $row['father_name'], $row['mother_name'], $row['mobile_no'], $row['email_cucet'], $row['total_marks'], );
            $i++;
        }

        return json_encode($responce);
    }

    public function actionAttendance()
    {
    	/**
    	* Getting school, dept and program data to show it in dropdown.
    	* This data is controller from Admin > School Tab.
    	*
    	*/
    	$program = Yii::$app->db->createCommand(
            "SELECT ap.id AS pid, asch.name AS sname, ad.name AS dname, ap.name AS pname
            FROM admin_schools AS asch
            LEFT JOIN admin_dept AS ad ON ad.schoolId=asch.id JOIN admin_program AS ap ON ap.deptId=ad.id
            WHERE ap.duration > 0
            ORDER BY pname, dname, sname"
        )
        ->queryAll();
        $programOpt = '<option role="option" value="0" selected>Choose From Schools > Center > Program To See Batched Created For Program.</option>';

    	foreach ($program as $sc) $programOpt .= "<option role='option' value='" . $sc['pid'] . "'>" . $sc['sname'] . " > " . $sc['dname'] . " > " . $sc['pname'] ."</option>";

        return $this->render('attendance', [
        	'programOpt' => $programOpt
        ]);
    }

    public function actionGet_application_no_autocomplete()
    {
        if(isset($_POST['application_no']) && isset($_POST['program_id'])) {
            $application_no = HtmlPurifier::process($_POST['application_no']);
            $program_id = $_POST['program_id'] *1;
            $autocomplete_array = array();

            $cucet_application_no = Yii::$app->db->createCommand(
            	"SELECT id, cucet_application_no, full_name
            	FROM cucet_result
            	WHERE program_id=:program_id AND cucet_application_no LIKE '%$application_no%' LIMIT 10"
            )
            ->bindParam(":program_id", $program_id)
            ->queryAll();

            foreach ($cucet_application_no as $value) {
                $autocomplete_array[] = array(
                    'id' => $value['id'],
                    'label' => trim($value['cucet_application_no'] . ' (' . $value['full_name'] . ')'),
                    'value' => trim($value['cucet_application_no'] . ' (' . $value['full_name'] . ')')
                );
            }

            return json_encode($autocomplete_array);
        }
        else return json_encode("illegal-attempt");
    }
    public function actionGet_cucet_attendance_data()
    {
        $program_id = $_POST['program_id'] *1;
        $uploaded_id = $_POST['uploaded_id'] *1;
        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);
            
    	if(!$sidx) $sidx = "name";
        if(!$sord) $sord = "ASC";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cucet_result WHERE program_id=:program_id AND id=:uploaded_id")
        ->bindParam(":program_id", $program_id)
        ->bindParam(":uploaded_id", $uploaded_id)
        ->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
 
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE program_id=:program_id AND id=:uploaded_id ORDER BY $sidx $sord LIMIT :st, :lt")
        	->bindParam(":program_id", $program_id)
	        ->bindParam(":uploaded_id", $uploaded_id)
            ->bindValue(":st", $start *1)
            ->bindValue(":lt", $limit *1)
            ->queryAll();

        foreach($result as $row) {
        	if($row['attendance'] == "PRESENT") $attendance = "PRESENT";
        	else $attendance = "ABSENT";
            $responce->rows[$i]['id']= $row['id'];
            $responce->rows[$i]['cell'] = array($row['cucet_application_no'], $row['full_name'], $row['father_name'], $row['mother_name'], $row['mobile_no'], $row['email_cucet'], $row['total_marks'], $attendance);
            $i++;
        }

        return json_encode($responce);
    }

    public function actionSave_cucet_attendance_data()
    {
        $oper = HtmlPurifier::process($_POST['oper']);
        if(isset($_POST["program_id"])) $program_id = HtmlPurifier::process($_POST['program_id']);
        if(isset($_POST["uploaded_id"])) $uploaded_id = HtmlPurifier::process($_POST['uploaded_id']);
        if(isset($_POST["attendance_id"])) $attendance_id = HtmlPurifier::process($_POST['attendance_id']);
        $msg = "";

        switch ($oper) {
            case "add":
                $msg .= "added";
                break;
            case "edit":
                \Yii::$app->db->createCommand("UPDATE cucet_result SET attendance='PRESENT' WHERE program_id=:program_id AND id=:attendance_id AND id=:uploaded_id")
                ->bindParam(':program_id', $program_id)
                ->bindParam(':attendance_id', $attendance_id)
                ->bindParam(':uploaded_id', $uploaded_id)
                ->execute();
                
                $msg .= "data-updated";
                break;
            case "del":
                $msg .= "del";
                break;
            default:
                $msg .= "no-match-found";
        }
        return $msg;
    }

    public function actionMerit()
    {
    	/**
    	* Getting school, dept and program data to show it in dropdown.
    	* This data is controller from Admin > School Tab.
    	*
    	*/
    	$program = Yii::$app->db->createCommand(
            "SELECT ap.id AS pid, asch.name AS sname, ad.name AS dname, ap.name AS pname
            FROM admin_schools AS asch
            LEFT JOIN admin_dept AS ad ON ad.schoolId=asch.id JOIN admin_program AS ap ON ap.deptId=ad.id
            WHERE ap.duration > 0
            ORDER BY pname, dname, sname"
        )
        ->queryAll();
        $programOpt = '<option role="option" value="0" selected>Choose From Schools > Center > Program To See Batched Created For Program.</option>';

    	foreach ($program as $sc) $programOpt .= "<option role='option' value='" . $sc['pid'] . "'>" . $sc['sname'] . " > " . $sc['dname'] . " > " . $sc['pname'] ."</option>";

        return $this->render('merit', [
        	'programOpt' => $programOpt
        ]);
    }
    public function actionGet_merit_general()
    {
        $program_id = $_POST['program_id'] *1;
        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);
            
    	if(!$sidx) $sidx = "name";
        if(!$sord) $sord = "ASC";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cucet_result WHERE program_id=:program_id AND (user_id>0 OR attendance IS NOT NULL)")->bindParam(":program_id", $program_id)->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
 
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE program_id=:program_id AND (user_id>0 OR attendance IS NOT NULL) ORDER BY $sidx $sord LIMIT :st, :lt")
        	->bindParam(":program_id", $program_id)
            ->bindValue(":st", $start *1)
            ->bindValue(":lt", $limit *1)
            ->queryAll();

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];
            $responce->rows[$i]['cell'] = array($row['full_name'], $row['cucet_application_no'], $row['total_marks']);
            $i++;
        }

        return json_encode($responce);
    }

    public function actionGet_merit_st()
    {
        $program_id = $_POST['program_id'] *1;
        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);
            
    	if(!$sidx) $sidx = "name";
        if(!$sord) $sord = "ASC";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cucet_result WHERE program_id=:program_id AND category='ST' AND (user_id>0 OR attendance IS NOT NULL)")->bindParam(":program_id", $program_id)->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
 
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE program_id=:program_id AND category='ST' AND (user_id>0 OR attendance IS NOT NULL) ORDER BY $sidx $sord LIMIT :st, :lt")
        	->bindParam(":program_id", $program_id)
            ->bindValue(":st", $start *1)
            ->bindValue(":lt", $limit *1)
            ->queryAll();

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];
            $responce->rows[$i]['cell'] = array($row['full_name'], $row['cucet_application_no'], $row['total_marks']);
            $i++;
        }

        return json_encode($responce);
    }

    public function actionGet_merit_sc()
    {
        $program_id = $_POST['program_id'] *1;
        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);
            
    	if(!$sidx) $sidx = "name";
        if(!$sord) $sord = "ASC";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM cucet_result WHERE program_id=:program_id AND category='SC' AND (user_id>0 OR attendance IS NOT NULL)")->bindParam(":program_id", $program_id)->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;
 
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE program_id=:program_id AND category='SC' AND (user_id>0 OR attendance IS NOT NULL) ORDER BY $sidx $sord LIMIT :st, :lt")
        	->bindParam(":program_id", $program_id)
            ->bindValue(":st", $start *1)
            ->bindValue(":lt", $limit *1)
            ->queryAll();

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];
            $responce->rows[$i]['cell'] = array($row['full_name'], $row['cucet_application_no'], $row['total_marks']);
            $i++;
        }

        return json_encode($responce);
    }
    public function actionGet_student_details()
    {
       if(isset($_POST['rowKey'])) {
            $id = $_POST['rowKey'] *1;

            $result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE id=:id")
                ->bindParam(":id", $id)            
                ->queryOne();

            $mpdf= new mPDF();
            $stylesheet = file_get_contents('css/bootstrap.css');
            $stylesheet2 = file_get_contents('css/myforms.css');

            $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($stylesheet2,\Mpdf\HTMLParserMode::HEADER_CSS);
           $html ='<html>
               <head>
                    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

               </head>
               <div class="myform">
 
                  <table>
                     <tbody>
                        <tr >
                           <td >
                              <table>
                                 <tbody>
                                    <tr>
                                       <td><img src="https://www.cusb.ac.in/images/cusb-files/interface/cusb-hn-en-logo-style5.png"  height="60" width="60" /></td>   

                                    </tr>
                                    <tr><td>दक्षिण बिहार केन्‍द्रीय विश्‍वविद्यालयदक्षिण बिहार केन्‍द्रीय विश्‍वविद्यालय</td></tr>
                                    <tr>
                                       <td style="text-align: center; vertical-align: middle;"><strong style="font-size: 18px;"> CENTRAL UNIVERSITY OF SOUTH BIHAR</strong></td>
                                    </tr>
                                    </tr>
                                    <tr>
                                       <td style="text-align: center; vertical-align: middle;">
                                            <span style="font-size: 10px;">Formerly Central University of Bihar, and since changed under The Central Universities (Amendment) Act, 2014)
                                            </span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;" >
                                            <span style="font-size: 11px;">
                                                SH-7, Gaya-Panchanpur Road, Village : Karhara, Post: Fatehpur, P.S : Tekari, District : Gaya (Bihar) Pin-824236
                                            </span>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;" >
                                            <span style="font-size: 10px;"> website : www.cusb.ac.in </span>
                                        </td> 
                                    </tr>                                 
                                    <tr>
                                       <td style="text-align: center; vertical-align: middle;">
                                            <span style="background-color: #000000; color: #ffffff;"><strong> ENROLMENT FORM&nbsp; &nbsp;</strong></span>
                                        </td>
                                    </tr>                                 
                                    <tr>
                                       <td style="text-align: center; vertical-align: middle;">
                                          <p><span style="color: #000000;"><strong>Admission in the Academic Session: 2019 - 2020</strong></span></p>
                                          <p><span style="color: #000000; font-size: 10px;">Note: All entries in this form should be made by the students in his/her own handwriting with ball pen and legibly written. No column should be left blank. Please tick in the box whichever is applicable.</span></p>
                                       </td>
                                       <td>Affix latest Colour passport Size Photograph</td>
                                    </tr>

                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr >
                                       <td ><span style="color: #000000;">CUCET Application No</span></td>
                                       <td >&nbsp;</td>
                                       <td >
                                          <p>Enrolment No. (to be GIven by the Office)</p>
                                       </td>
                                    </tr>
                                    <tr >
                                       <td >
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td > AAAAAAAAA</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                       <td >&nbsp;</td>
                                       <td >
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td >&nbsp;1</td>
                                                   <td >2</td>
                                                   <td >3</td>
                                                   <td >4</td>
                                                   <td >5</td>
                                                   <td >6</td>
                                                   <td >7</td>
                                                   <td >8</td>
                                                   <td >9</td>
                                                   <td >0</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td>Admitted in Program : </td>
                                       <td>BBBBBBBBBBBBBBBBBB</td>
                                    </tr>
                                    <tr>
                                       <td >Subject:</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td >Department of&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td >School of&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td >Admission type (in the case of Ph.d. only):Regular / Part Time /External</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >1. Name of the Student (in Capital letters as&nbsp; in the SSC/Matriculation</td>
                                       <td >'.$result['full_name'].'</td>
                                    </tr>
                                    <tr>
                                       <td >In English</td>
                                       <td >'.$result['full_name'].'</td>
                                    </tr>
                                    <tr>
                                       <td >In hindi</td>
                                       <td >'.$result['full_name'].'</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >2. Date of Birth</td>
                                       <td >'.$result['dob'].'</td>
                                       <td >3. Gender</td>
                                       <td >'.$result['gender'].'</td>
                                    </tr>
                                    <tr>
                                       <td >4. Blood Group</td>
                                       <td >xxxxxxxxxx</td>
                                       <td >5. Maritial Status</td>
                                       <td >xxxxxxxxx</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >6. Category</td>
                                       <td >'.$result['category'].'</td>
                                    </tr>
                                    <tr>
                                       <td >7. Whether Ex.Service Men</td>
                                       <td >xxx</td>
                                    </tr>
                                    <tr>
                                       <td >7. Wards of defence personnel</td>
                                       <td >'.$result['defence_per'].'</td>
                                    </tr>
                                    <tr>
                                       <td >7. Kashmiri Migrants</td>
                                       <td >'.$result['kashmiri_mig'].'</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >8. Physically Challenged&nbsp;</td>
                                       <td >'.$result['phy_handicap'].'</td>
                                       <td >'.$result['handicap_type'].'</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td >9. Religion</td>
                                       <td >'.$result['religion'].'</td>
                                       <td >10. Domicile State</td>
                                       <td >'.$result['domicile_state'].'</td>
                                    </tr>
                                    <tr>
                                       <td >11. Nationality</td>
                                       <td >'.$result['nationality'].'</td>
                                       <td >12. Citizenship</td>
                                       <td >XXXXXXX</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >13. Annual Income (For the prexeding financial year i.e 01.04.2018 to 31.03.2019.)</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr >
                                       <td >Father \'s Name:</td>
                                       <td >'.$result['father_name'].'</td>
                                       <td >Occupation:</td>
                                       <td >xxxxx</td>
                                       <td >Annual Income&nbsp;₹</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr >
                                       <td >Mother \'s Name :</td>
                                       <td >'.$result['father_name'].'</td>
                                       <td >Occupation:</td>
                                       <td >&nbsp;</td>
                                       <td >Annual Income&nbsp;₹</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr >
                                       <td >
                                          <p>Guardian Name:</p>
                                          <p>(if both parents are not alive)</p>
                                       </td>
                                       <td >&nbsp;</td>
                                       <td >Occupation:</td>
                                       <td >&nbsp;</td>
                                       <td >Annual Income ₹</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >14. Native Place</td>
                                       <td >xxxxxxxxxx</td>
                                       <td >District</td>
                                       <td >'.$result['district'].'</td>
                                       <td >State</td>
                                       <td >xxxxxxxx</td>
                                    </tr>
                                    <tr>
                                       <td >Distance From Gaya</td>
                                       <td >&nbsp;</td>
                                       <td >K.M</td>
                                       <td >Nearest ailway&nbsp;</td>
                                       <td >Station</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >15. Do you belongs to LBC&nbsp;</td>
                                       <td >xxxxxxx</td>
                                       <td >if Yes, state the Group</td>
                                       <td >xxxxxxxxxxxxxxx</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >16. Do you belongs to any miniority community? Yes / No&nbsp; if Yes, State the Following</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >Community :</td>
                                       <td >xxxxxxxxxxxx</td>
                                       <td >Religion:</td>
                                       <td >xxxxxxxxxxxxxxxxxx</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >&nbsp; &nbsp; &nbsp;Note: the Information at item 14 &amp; 15 is for statistical purpose only.</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >17. Do you require hostel accommodation :</td>
                                       <td >xxxxxxxxxxxxxxxx</td>
                                    </tr>
                                    <tr>
                                       <td >18. Are you interested to enroll in NSS&nbsp; &nbsp; &nbsp; :</td>
                                       <td >xxxxxxxxxxxxxxxx</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr >
                                       <td >19. Any scholarship / any other financial support during your previous study, if so, give detail:</td>
                                    </tr>
                                    <tr >
                                       <td >
                                          <p>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
                                          <p>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >20. Academic record from Matriculation onwards</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td ><strong>Examinitaion&nbsp;</strong></td>
                                       <td ><strong>Board / University</strong></td>
                                       <td ><strong>Subjects Studied</strong></td>
                                       <td >&nbsp;<strong>Year of Passing</strong></td>
                                       <td >Max marks</td>
                                       <td >&nbsp;Marks Obtained&nbsp;</td>
                                       <td >% of Marks</td>
                                       <td >Grade / Div / Class</td>
                                    </tr>
                                    <tr>
                                       <td >Matric / SSC / equivalent</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>Intermediate / HSC (10+2)</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>Bachelor \'s Degree</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>Masters Degree</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>M.Phill /M.Tech</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>Any Other Degree / diploma / BE / B.Tech / MCA etc.</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td>NET / SLET / GATE / JRF</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td><strong>Note: Attested copies of all the sertificates to be furnished at the time of admission along with the original certificates.</strong></td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table>
                                 <tbody>
                                    <tr >
                                       <td >
                                          21. Details of present or previous employmet, if any
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td >Organisation and Address</td>
                                                   <td >Position Held</td>
                                                   <td >&nbsp;From - To</td>
                                                   <td>Nature of Duties</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td >&nbsp;</td>
                                                   <td >&nbsp;</td>
                                                   <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td >&nbsp;</td>
                                                   <td >&nbsp;</td>
                                                   <td>&nbsp;</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td >If in service, enclose a conduct certificate from your present employer and a "no objection Certificate" for pursuing studies in the Centrl University of Bihar.</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >22. (a) Was any disciplinary action taken against you during your academic career so far? If so, give details (You may attach separate sheet)</td>
                                    </tr>
                                    <tr>
                                       <td >xxxxxxxx</td>
                                    </tr>
                                    <tr>
                                       <td >(b) Were you ever convicted by a Court of law on criminal or any other charge? If so, give details (You may attach separate sheet)</td>
                                    </tr>
                                    <tr>
                                       <td >xxxxxxx&nbsp;</td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >
                                          <p>23. Any other relevant information</p>
                                          <table>
                                             <tbody>
                                                <tr >
                                                   <td><span style="background-color: #000000; color: #ffffff;"><strong>Permanent Address&nbsp;</strong></span></td>
                                                   <td >&nbsp;<span style="color: #ffffff; background-color: #000000;"><strong>Present Mailing Address</strong></span></td>
                                                </tr>
                                                <tr >
                                                   <td >
                                                      <table>
                                                         <tbody>
                                                            <tr >
                                                               <td >&nbsp;</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >&nbsp;</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >P.O&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >Distt&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >Pin Code&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >Telephone No&nbsp; &nbsp; &nbsp; :</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >Mobile No&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr >
                                                               <td >Email&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                   <td >
                                                      <table >
                                                         <tbody>
                                                            <tr>
                                                               <td >&nbsp;</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >&nbsp;</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >P.O&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >Distt&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >Pin Code&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >Telephone No&nbsp; &nbsp; &nbsp; :</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >Mobile No&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td >Email&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</td>
                                                               <td >&nbsp;</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td >Contact Person in case of emergency&nbsp;</td>
                                                   <td >&nbsp;</td>
                                                   <td>Name</td>
                                                   <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                   <td >Relatioship</td>
                                                   <td s>XXXXXXXXXXXXX</td>
                                                   <td s>Phone Number</td>
                                                   <td>&nbsp;XXXXXXXXXXgggXX</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>                                 
                                    <tr>
                                       <td >24. <span style="text-decoration: underline;"><strong>DECLARATION BY THE STUDENT</strong></span></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          I hereby declare that all the information furnished by me in this enrolment form and in the documents enclosed are true, complete and correct. In case any information is found to be false or incorrect at any time (during or after completion of the course), this shall entail automatic cancellation of my admission if granted, and cancellation of the degree if awarded, besides rendering me liable to such action as the University may deem fit. In the event of any medical or other emergency, my parent/s guardian may be contacted at the address given in the form. I hereby accept the conditions stated above, in the instructions to the candidates for admission and in the Prospectus of the mentioned academic session. <br />I will abide by the rules and regulations with regard to semester wise registration and credit system adopted by the University. I hereby submit myself to the disciplinary jurisdiction of the Vice-Chancellor and the other authorities of the Central University of Bihar. I shall neither indulge myself or instigate any other student in ragging or create nuisance to the academic atmosphere of the University. In case of any act of misconduct on my part or any disciplinary proceeding against me, the University has freedom to inform my parents/guardian.&nbsp;
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td >Place : XXXXXXXXXXXXXXXXXX</td>
                                                   <td >&nbsp;</td>
                                                </tr>
                                                <tr>
                                                   <td >Date&nbsp; :XXXXXXXXXXXXXX</td>
                                                   <td >Signature Of the Student:</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>                                 
                                    <tr >
                                       <td >25 . <span style="text-decoration: underline;"><strong>DECLARATION BY THE FATHER / MOTHER /GUARDIAN</strong></span></td>
                                    </tr>
                                    <tr >
                                       <td>
                                           <p>My son/daughter/wife/ward/sri/smt./kumari. xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx is provisionally admitted to the University. I hereby undertake that I shall be responsible for payment of all his/her fees and other charges including any emergency, medical or other expenses incurred by the University. In case any information in this form and the documents enclosed are found to be false or incorrect at any time (during and after completion of the course), this shall entail automatic cancellation of my sons/daughters/wards admission if granted, and cancellation of the degree if awarded, besides rendering him/her liable to such action as the University may deem fit. I will also be responsible for his/her good conduct and behavior during the period of his/her stay in the University. Further, I may be contacted in the event of any emergency as determined by the University and I hereby promise that I will make myself present before the University authority at my own cost whenever the University requires my presence.
                                           </p>
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td >Place :&nbsp;</td>
                                                   <td >Signature:&nbsp;</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td >Full Name:</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td >Relationship with Student:</td>
                                                </tr>
                                                <tr>
                                                   <td >Date:&nbsp;</td>
                                                   <td >Contact :</td>
                                                </tr>                                             
                                                <tr>
                                                   <td >Note: The signature of the parent on this enrolment from will be considered as basis for all verifiacation purpose in the university.</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr >
                           <td >
                              <table>
                                 <tbody>
                                    <tr>
                                       <td ><strong>DECLARATION BY THE FATHER / MOTHER /GUARDIAN</strong></td>
                                    </tr>
                                    <tr>
                                       <td >
                                          <p>&nbsp;My son/daughter/wife/ward/sri/smt./kumari. '.$result['full_name'].' is provisionally admitted to the University. I hereby undertake that I shall be responsible for payment of all his/her fees and other charges including any emergency, medical or other expenses incurred by the University. In case any information in this form and the documents enclosed are found to be false or incorrect at any time (during and after completion of the course), this shall entail automatic cancellation of my sons/daughters/wards admission if granted, and cancellation of the degree if awarded, besides rendering him/her liable to such action as the University may deem fit. I will also be responsible for his/her good conduct and behavior during the period of his/her stay in the University. Further, I may be contacted in the event of any emergency as determined by the University and I hereby promise that I will make myself present before the University authority at my own cost whenever the University requires my presence.</p>
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td >Place :&nbsp;</td>
                                                   <td ">Signature:&nbsp;</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td ">Full Name:</td>
                                                </tr>
                                                <tr>
                                                   <td >&nbsp;</td>
                                                   <td ">Relationship with Student:</td>
                                                </tr>
                                                <tr>
                                                   <td >Date:&nbsp;</td>
                                                   <td ">Contact :</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table >
                                             <tbody>
                                                <tr>
                                                   <td>Note: The signature of the parent on this enrolment from will be considered as basis for all verifiacation purpose in the university.</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr class="item" >
                           <td ><span style="background-color: #000000; color: #ffffff;"><strong>For Office Use</strong></span></td>
                        </tr>
                        <tr class="item" >
                           <td >All the certificates and documents have been verified. The candidate may be provisionally admitted to the course</td>
                        </tr>
                        <tr class="item" >
                           <td >Checked and found in order</td>
                        </tr>
                        <tr class="total" >
                           <td >
                              &nbsp;
                              <table >
                                 <tbody>
                                    <tr>
                                       <td >Dealing Assitant</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >Verified</td>
                                       <td >&nbsp;</td>
                                       <td >Admitted / Rejected</td>
                                    </tr>
                                    <tr>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >&nbsp;</td>
                                       <td >Dy Registrar</td>
                                       <td >SO</td>
                                       <td >&nbsp;COE</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </html>';            
            
            $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output("/home/yash/http/yii2-cas/backend/web/export/MyFile.pdf");
            return true;
        }
        else return "illegal-attempt";
    }
}
