<?php
date_default_timezone_set('Asia/Kolkata');
$currentDate = date("Y-m-d");// current date
$yr = date("Y");
$yr_next = date("Y", strtotime(date("Y-m-d", strtotime($currentDate)) . " +1 year"));

\yii\bootstrap\BootstrapAsset::register($this);
$this->registerJsFile('@web/custom/js/counselling_mpdf.js', ['depends' => [common\assets\JqGridAsset::className()]]);

$result = Yii::$app->db->createCommand("SELECT * FROM cucet_result  WHERE id=:result_id")
    ->bindParam(":result_id", $result_id)            
    ->queryOne();
$program_id = $result['program_id'];
$dept_id = $result['dept_id'];
$school_id = $result['school_id'];

$school_details = Yii::$app->db->createCommand("
        SELECT ass.name AS school, ad.name AS dept, ap.name AS program
        FROM admin_schools AS ass
        JOIN admin_dept AS ad ON ad.schoolId=ass.id
        JOIN admin_program AS ap ON ap.deptId=ad.id
        WHERE ass.id=:school_id AND ad.id=:dept_id AND ap.id=:program_id"
    )
    ->bindParam(":school_id", $school_id)            
    ->bindParam(":dept_id", $dept_id)            
    ->bindParam(":program_id", $program_id)            
    ->queryOne();

?>
<p class="text-center pull-right">
    <strong>CENTRAL UNIVERSITY OF SOUTH BIHAR</strong><br>
    <span style="font-size: 10px;">Formerly Central University of Bihar, and since changed under The Central Universities (Amendment) Act, 2014)</span><br>
    <span style="font-size: 11px;">
        SH-7, Gaya-Panchanpur Road, Village : Karhara, Post: Fatehpur, P.S : Tekari, District : Gaya (Bihar) Pin-824236
    </span><br>
    <span style="font-size: 10px;"> website : www.cusb.ac.in </span><br>
    <span style="background-color: #000000; color: #ffffff;"><strong> ENROLMENT FORM</strong></span>
</p>
<br>
<p class="text-center"><span style="color: #000000;"><strong>Admission in the Academic Session: <?= $yr . '-' . $yr_next ?></strong></span></p>
<table>
    <tr>
       <td>CUCET APPLICATION NO</strong></td>
       <td>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>
       <td>ENROLLMENT NO (to be given by the Office)</td>
    </tr>
    <tr >
       <td><strong><?= $result['cucet_application_no'] ?></strong></td>
       <td>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</td>
       <td>
            <div style="border:0.2px solid #000;">____________________________________________</div>
        </td>
    </tr>
</table>
<br>
<p><strong>Admitted in - Program : </strong><?= $school_details['school'] ?>,<br><?= $school_details['dept'] ?> in <?= $school_details['school'] ?>.</p>
<p><strong>Date of admission : </strong><?= date("Y-m-d") ?>.<br><strong>Time of printing of form at the time of attendance : </strong><?= date("g:i:s A") ?></p>
<p><strong>Admission Type (in case of Ph.D only): </strong> Regular / Part Time / External ________________________</p>
<?php
Yii::$app->language = 'pl';
?>
<ol>
    <li>
        Name of the Student (in Capital letters as in the SSC/Matriculation :
        <br>In English : <?= ucwords($result['full_name']) ?>
        <br>In Hindi : 
    </li>
    <li>Date of Birth : <?= $result['dob'] ?></li>
    <li>Gender : <?= $result['gender'] ?></li>
    <li>Blood Group : _______</li>
    <li>Marital Status : Married (____) Unmarried (____) Divorced (____) [Please check the box.]</li>
    <li>Category <?= $result['category'] ?></li>
    <li>Weather : Ex. Service Men (____), Wards of Defence personnel <?= $result['defence_per'] ?>, Kashmiri Migrants <?= $result['kashmiri_mig'] ?>, NA (_____)</li>
    <li>In case of Physically Challenged : <?php echo $result['phy_handicap']; if(strcmp($result['phy_handicap'], "YES")==0) echo ", disablity type : " . $result['handicap_type']; ?></li>
    <li>Religion : <?= $result['religion'] ?></li>
    <li>State of Domicile : <?= $result['domicile_state'] ?></li>
    <li>Nationality : <?= $result['nationality'] ?></li>
    <li>Citizenship : ______________________</li>
    <li>
        Annual Income (For the prexeding financial year i.e 01.04.2018 to 31.03.2019.)<br>
        <table class="table table-bordered table-condensed">
            <tr>
               <td  width="35%" height="50">Father's Name : <?= $result['father_name'] ?></td>
               <td width="30%"> Occupation :  </td>
               <td> Annual Income (₹) :</td>
            </tr>
            <tr>
               <td  width="35%" height="50">Mother'sName:<?= $result['mother_name']?></td>
               <td  width="30%">Occupation : </td>
               <td>AnnualIncome(₹) : </td>
            </tr>
            <tr>
               <td  width="35%" height="50">
                  <br><p>GuardianName :<br>(if both parents are not alive)</p>
               </td>
               <td width="30%">Occupation : </td>
               <td>AnnualIncome (₹) : </td>
            </tr>
        </table>        
    </li>
    <li>
        <p>Native Place : ________________________________, District : <?= $result['district'] ?>, State : ___________________________</p>
        <p >Distance From Gaya (in K.M) ____________, Nearest ailway Station (Station Code): ____________</p>
    </li>
    <li>Do you belong to LBC (Yes / No) _________ if Yes, state the <strong>Group</strong>_______________________________</li>
    <li>
        <p>
            Do you belong to miniority community? (Yes / No) _________.<br>If yes, please state the following :
            Community ___________________________, Religion <?= $result['religion'] ?>.<br>
        </p>
        <p>Note: the Information at item 14 &amp; 15 is for statistical purpose only.</p>
    </li>
    <li>Do you require hostel accommodation : <?php if($result['hostel'] == "YES") echo "YES"; else echo "NO"; ?></li>
    <li>Are you interested to enroll in NSS (Yes / No) : _________.</li>
    <br>
    <li>Any scholarship / any other financial support during your previous study, if so, give detail :<br><br>____________________________________________________________________________________________________<br><br>____________________________________________________________________________________________________.</li>
    <li>
        Academic record from Matriculation onwards
        <br>
        <table class="table table-bordered table-condensed">
            <tr style="text-align:right">
              <td width="12%" height="70"><strong>Examinitaion</strong></td>
              <td width="23%"><strong>Board / University</strong></td>
              <td width="20%"><strong>Subjects Studied</strong></td>
              <td width="9%"><strong>YP</strong></td>
              <td width="8%"><strong>MM</strong></td>
              <td width="8%"><strong>MO</strong></td>
              <td width="10%"><strong>Marks (%)</strong></td>
              <td width="10%"><strong>Grade / Div / Class</strong></td>
            </tr>
            <tr>
              <td height="70"><strong>Matric / SSC / equivalent</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>Intermediate / HSC (10+2)</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>Bachelor \'s Degree</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>Masters Degree</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>M.Phill /M.Tech</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>Any Other Degree / diploma / BE / B.Tech / MCA etc.</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="70"><strong>NET / SLET / GATE / JRF</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
        </table>
        <p>
            <strong>Note : YP - Passing Year, MM - Maximum Marks, MO - Marks Obtained.</strong>
            <strong>Note : Attested copies of all the sertificates to be furnished at the time of admission along with the original certificates.</strong>
        </p>
    </li>
    <li>
        Details of present or previous employmet, if any
        <table class="table table-bordered table-condensed">
            <tr>
              <td height="50">Organisation and Address</td>
              <td>Position Held</td>
              <td>From - To</td>
              <td>Nature of Duties</td>
            </tr>
            <tr>
              <td height="50"></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td height="50"></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
        </table>
        <p><strong>If in service, enclose a conduct certificate from your present employer and a "no objection Certificate" for pursuing studies in the Centrl University of Bihar.</strong></p>
    </li>
    <li>
        (a) Was any disciplinary action taken against you during your academic career so far? If so, give details (You may attach separate sheet) ____________________________________________________________________________________________________<br>
        (b) Were you ever convicted by a Court of law on criminal or any other charge? If so, give details (You may attach separate sheet) ____________________________________________________________________________________________________
    </li>
    <li>
        Any other relative information ____________________________________________________________________________________________________
        <br>
        <br>
        <table class="table table-bordered table-condensed">
            <tr>
                <td width="50%" class="text-center"><br><span style="color: #ffffff; background-color: #000000;"><strong>Permanent Address<br><br><br></strong></span></td>
                <td class="text-center"><br><span style="color: #ffffff; background-color: #000000;"><strong>Present Mailing Address<br><br><br></strong></span></td>
            </tr>
            <tr >
                <td class="text-center"><br><?= $result['present_address1'] ?><br><br><br></td>
                <td class="text-center"><br><?= $result['present_address2'] ?><br><br><br></td>
            </tr>
        </table>
        <p>Contact person in case of emergency -<br>Name __________________________, Relationship _________________, Contact Details _________________.</p>
    </li>
    <li>
        <p>
            <strong>DECLARATION BY THE STUDENT</strong><br>
            <small>I hereby declare that all the information furnished by me in this enrolment form and in the documents enclosed are true, complete and correct. In case any information is found to be false or incorrect at any time (during or after completion of the course), this shall entail automatic cancellation of my admission if granted, and cancellation of the degree if awarded, besides rendering me liable to such action as the University may deem fit. In the event of any medical or other emergency, my parent/s guardian may be contacted at the address given in the form. I hereby accept the conditions stated above, in the instructions to the candidates for admission and in the Prospectus of the mentioned academic session. <br />I will abide by the rules and regulations with regard to semester wise registration and credit system adopted by the University. I hereby submit myself to the disciplinary jurisdiction of the Vice-Chancellor and the other authorities of the Central University of Bihar. I shall neither indulge myself or instigate any other student in ragging or create nuisance to the academic atmosphere of the University. In case of any act of misconduct on my part or any disciplinary proceeding against me, the University has freedom to inform my parents/guardian.</small>
        </p>
        <p class="text-right"><br>Signature Of the Student</p>
    </li>
    <li>
        <p>
            <small><strong>DECLARATION BY THE FATHER / MOTHER /GUARDIAN</strong><br>
            My son/daughter/wife/ward/sri/smt./kumari. ____________________________ is provisionally admitted to the University. I hereby undertake that I shall be responsible for payment of all his/her fees and other charges including any emergency, medical or other expenses incurred by the University. In case any information in this form and the documents enclosed are found to be false or incorrect at any time (during and after completion of the course), this shall entail automatic cancellation of my sons/daughters/wards admission if granted, and cancellation of the degree if awarded, besides rendering him/her liable to such action as the University may deem fit. I will also be responsible for his/her good conduct and behavior during the period of his/her stay in the University. Further, I may be contacted in the event of any emergency as determined by the University and I hereby promise that I will make myself present before the University authority at my own cost whenever the University requires my presence.</small><br>
            <span class="text-center">Signature <br>Full Name:<br>Relationship with Student:<br>Contact No:<br>Date:</span></p>
        <p>Note: The signature of the parent on this enrolment from will be considered as basis for all verification purpose in the university.</p>
    </li>
    <p class="text-center">
        <span style="background-color: #000000; color: #ffffff;"><strong>For Office Use</strong></span><br>
        All the certificates and documents have been verified. The candidate may be provisionally admitted to the course checked and found in order.
        <table class="table">
            <tr>
              <td width="25%"><br><br><br><br>Dealing Assitant</td>
              <td width="50%"><br><br><br><br>Verified</td>
              <td  width="25%"><br><br><br><br>Admitted / Rejected</td>
            </tr>
            <tr>
              <td ><br><br><br><br></td>
              <td ><br><br><br><br><strong>Dy Registrar / Assistant Registrar / SO</strong></td>
              <td ><br><br><br><br><strong>COE</strong></td>
            </tr>
        </table>
    </p>

</ol> 
