<?php
    $this->title = 'Attendance';

    use yii\web\View;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use common\assets\JqGridAsset;
    use common\assets\IntroAsset;
    $this->registerCss("
        .positive-row { background-color: #7BF27B;}
        .negative-row { background-color: #FFA6A6;}      
    ");

    $this->registerJsFile('@web/custom/js/counselling_attendance.js', ['depends' => [JqGridAsset::className()]]);
?>
<div class="col-xs-18 col-sm-12 col-md-12">
    <form class="form-horizontal">
        <div class="col-md-6 form-group">
            <select name="program_id" id="program_id" class="form-control">
                <?php echo $programOpt; ?>
            </select>
        </div>
        <input type="hidden" id="uploaded_id" value=0>
        <div class="col-md-6 form-group">
            <label for="application_no" class="col-xs-18 col-md-2 col-sm-2 control-label">Application No.</label>
            <div class="col-xs-18 col-md-10 col-sm-10">
                <input type="text" class="form-control" id="application_no" placeholder="Type CUCET application number">
            </div>    
        </div>
    </form>
</div>
<br>
<div class="col-xs-18 col-sm-12 col-md-12">
    <table id="attendance"></table>
    <div id="pager_attendance"></div>
</div>

<div id="overlay-loading"></div>
<div id="loading-image">
    <img src="<?= Yii::$app->homeUrl . "custom/images/loading.gif" ?>" />
</div>