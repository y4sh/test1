<?php
    $this->title = 'Merit List';

    use yii\web\View;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use common\assets\JqGridAsset;
    use common\assets\IntroAsset;

    $this->registerJsFile('@web/custom/js/counselling_merit.js', ['depends' => [JqGridAsset::className()]]);
?>
<div class="col-xs-18 col-sm-12 col-md-12">
    <p><mark>CUCET MERIT LIST FOR CANDIDATE WHO SIGNED UP FOR ON ERP PORTAL!!!</mark> DATA NOT SHOWING FOR NON USER OF ERP PORTAL.</p>
    <form>
        <div class="form-inline">
            <select name="program" id="programId" class="form-control">
                <?= $programOpt ?>
            </select>
        </div>
    </form>
    <br />
</div>
<div class="col-xs-18 col-sm-7 col-md-7">
    <table id="batch"  class="pull-left"></table>
    <div id="pager_batch"></div><br />
</div>
<div class="clearfix"></div>
<div class="col-xs-18 col-sm-4 col-md-4">
    <table id="general"  class="pull-left"></table>
    <div id="pager_general"></div><br />
</div>
<div class="col-xs-18 col-sm-4 col-md-4">
    <table id="st"  class="pull-left"></table>
    <div id="pager_st"></div><br />
</div>
<div class="col-xs-18 col-sm-4 col-md-4">
    <table id="sc"  class="pull-left"></table>
    <div id="pager_sc"></div><br />
</div>

<div id="overlay-loading"></div>
<div id="loading-image">
    <img src="<?= Yii::$app->homeUrl . "custom/images/loading.gif" ?>" />
</div>