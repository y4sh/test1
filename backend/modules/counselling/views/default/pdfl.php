<?php
    $this->title = 'Merit List';

    use yii\web\View;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use common\assets\JqGridAsset;
    use common\assets\IntroAsset;
?>
  <html>
            <head>
            <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
            <style type="text/css">
                <!-- span.cls_002 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(63, 2, 4);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_002 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(63, 2, 4);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_004 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 191);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_004 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 191);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_003 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_003 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_005 {
                    font-family: "OpenSymbol", serif;
                    font-size: 2.0px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_005 {
                    font-family: "OpenSymbol", serif;
                    font-size: 2.0px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_008 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_008 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_006 {
                    font-family: Arial, serif;
                    font-size: 12.1px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_006 {
                    font-family: Arial, serif;
                    font-size: 12.1px;
                    color: rgb(0, 0, 0);
                    font-weight: normal;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_009 {
                    font-family: Arial, serif;
                    font-size: 5.6px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                div.cls_009 {
                    font-family: Arial, serif;
                    font-size: 5.6px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: normal;
                    text-decoration: none
                }
                
                span.cls_010 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: italic;
                    text-decoration: none
                }
                
                div.cls_010 {
                    font-family: Arial, serif;
                    font-size: 7.0px;
                    color: rgb(0, 0, 0);
                    font-weight: bold;
                    font-style: italic;
                    text-decoration: none
                }
                
                -->
            </style>   
        </head>

        <body>
        <div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
            <div style="position:absolute;left:185.90px;top:32.40px" class="cls_002">
                <span class="cls_002">CENTRAL UNIVERSITY OF SOUTH BIHAR</span>
            </div>
            <div style="position:absolute;left:85.60px;top:46.40px" class="cls_004">
                <span class="cls_004">(Formerly Central University of Bihar, and since changed under The Central Universities (Amendment) Act, 2014)</span>
            </div>
            <div style="position:absolute;left:83.70px;top:63.30px" class="cls_004">
                <span class="cls_004">P</span>
                <span class="cls_003">SH-7, Gaya-Panchanpur Road, Village : Karhara, Post: Fatehpur, P.S : Tekari, District : Gaya (Bihar) Pin-824236</span>
            </div>
            <div style="position:absolute;left:219.80px;top:77.40px" class="cls_004">
                <span class="cls_004">website : </span>
                <A HREF="http://www.cusb.ac.in/">www.cusb.ac.in</A>
            </div>
            <div style="position:absolute;left:58.20px;top:101.00px" class="cls_003">
                <span class="cls_003">Note: All entries in this form should be made by the students in his/her own handwriting with ball</span>
            </div>
            <div style="position:absolute;left:390px;top:101.00px" class="cls_003">
                <span class="cls_003">Affix latest colour Passport</span>
            </div>
            <div style="position:absolute;left:58.20px;top:109.10px" class="cls_003">
                <span class="cls_003">pen  and  legibly  written.  No  column  should  be  left  blank.  Please  tick  in  the  box  whichever  is</span>
            </div>
            <div style="position:absolute;left:390px;top:109.10px" class="cls_003">
                <span class="cls_003">size Photograph here and </span>
            </div>
            <div style="position:absolute;left:58.20px;top:117.10px" class="cls_003">
                <span class="cls_003">applicable.</span>
            </div>
            <div style="position:absolute;left:390px;top:117.10px" class="cls_003">
                <span class="cls_003">sign below in the space provided</span>
            </div>  
            <div style="position:absolute;left:476.00px;top:131.40px" class="cls_005">
                <span class="cls_005">•</span>
            </div>
            <div style="position:absolute;left:58.20px;top:134.00px" class="cls_003">
                <span class="cls_003">CUCET Application No.</span>
            </div>
            <div style="position:absolute;left:209.00px;top:134.00px" class="cls_003">
                <span class="cls_003">Enrolment No. (to be given by the office)</span>
            </div>
            <div style="position:absolute;left:58.20px;top:182.70px" class="cls_003">
                <span class="cls_003">Admitted in-Programme :</span>
                <span class="cls_008">......................</span>
            </div>
            <div style="position:absolute;left:233.49px;top:182.70px" class="cls_008">      
                <span class="cls_003">   Subject:</span>
                <span class="cls_003">..............</span>
            </div>
            <div style="position:absolute;left:58.20px;top:196.70px" class="cls_003">
                <span class="cls_003">Department of :</span>
                <span class="cls_008">.................................................................................................</span>
            </div>      
            <div style="position:absolute;left:58.20px;top:210.80px" class="cls_003">
                <span class="cls_003">School of :</span>
                <span class="cls_008">........................................................................</span>
            </div>
            <div style="position:absolute;left:250.28px;top:210.80px" class="cls_008">      
                <span class="cls_003"> Date of Admission :</span>
                <span class="cls_008">........................................................................</span>
            </div>
            <div style="position:absolute;left:58.20px;top:224.80px" class="cls_003">
                <span class="cls_003">Admission type (in the case of Ph.D. only): Regular / Part-Time / External</span>
            </div>
            <div style="position:absolute;left:58.20px;top:238.40px" class="cls_006">
                <span class="cls_006">1. Name of the Student (in CAPITAL letters as in the SSC/Matriculation) :</span>
            </div>
            <div style="position:absolute;left:58.20px;top:266.30px" class="cls_006">
                <span class="cls_006">In English :</span>
            </div>
            <div style="position:absolute;left:58.20px;top:294.30px" class="cls_006">
                <span class="cls_006">In Hindi :</span>
            </div>
            <div style="position:absolute;left:58.20px;top:325.60px" class="cls_003">
                <span class="cls_003">2. Date of Birth :</span>
                <span class="cls_008">..................      </span>
                <span class="cls_003">3. Gender: Male Female Transgender</span>
            </div>
            <div style="position:absolute;left:58.20px;top:339.60px" class="cls_003">
                <span class="cls_003">4. Blood Group :</span>
            </div>
            <div style="position:absolute;left:153.66px;top:339.60px" class="cls_003">
                <span class="cls_003">5. Marital Status: Married Unmarried Divorced</span>
            </div>
            <div style="position:absolute;left:58.20px;top:353.80px" class="cls_003">
                <span class="cls_003">6. Category</span>
                <span class="cls_009">
                    <sup>*</sup>
                </span>
                <span class="cls_003">: </span>
                <span class="cls_010">GEN OBC (Creamy Layer) OBC (Non-Creamy Layer) SC ST</span>
            </div>
            <div style="position:absolute;left:58.20px;top:369.10px" class="cls_003">
                <span class="cls_003">7. Whether: Ex. Service Men Wards of defence personnel Kashmiri Migrants NA</span>
            </div>
            <div style="position:absolute;left:58.20px;top:383.10px" class="cls_003">
                <span class="cls_003">8. In case of Physically Challenged, disability type: Orthoped Blind Deaf-Mute</span>
            </div>
            <div style="position:absolute;left:58.20px;top:397.20px" class="cls_003">
                <span class="cls_003">9. Religion : </span>
                <span class="cls_008">..................</span>
            </div>
            <div style="position:absolute;left:139.09px;top:397.20px" class="cls_008">
                <span class="cls_008">............................</span>
                <span class="cls_003">.. 10. State of Domicile :</span>
                <span class="cls_008">....................</span>
            </div>
            <div style="position:absolute;left:293.57px;top:397.20px" class="cls_008">
                <span class="cls_008">.......................</span>
            </div>
            <div style="position:absolute;left:58.20px;top:411.20px" class="cls_003">
                <span class="cls_003">11. Nationality: Indian / Foreign National 12. Citizenship : Indian / Foreign National</span>
            </div>
            <div style="position:absolute;left:58.20px;top:425.30px" class="cls_003">
                <span class="cls_003">13. Annual Income (For the preceding financial year i.e. 01.04.2018 to 31.03.2019.)</span>
            </div>
            <div style="position:absolute;left:58.20px;top:439.30px" class="cls_003">
                <span class="cls_003">Father's Name : Occupation: Annual Income `</span>
            </div>
            <div style="position:absolute;left:58.20px;top:453.40px" class="cls_003">
                <span class="cls_003">Mother's Name : Occupation: Annual Income `</span>
            </div>
            <div style="position:absolute;left:58.20px;top:467.40px" class="cls_003">
                <span class="cls_003">Guardian's Name : Occupation: Annual Income `</span>
            </div>
            <div style="position:absolute;left:58.20px;top:481.50px" class="cls_003">
                <span class="cls_003">(if both parents are not alive)</span>
            </div>
            <div style="position:absolute;left:58.20px;top:495.50px" class="cls_003">
                <span class="cls_003">14. Native Place: District: State:</span>
            </div>
            <div style="position:absolute;left:58.20px;top:509.60px" class="cls_003">
                <span class="cls_003">Distance from Patna k.m. nearest railway Station:</span>
            </div>
            <div style="position:absolute;left:58.20px;top:523.60px" class="cls_003">
                <span class="cls_003">15. Do you belong to LBC: Yes / No if Yes, state the Group:</span>
            </div>
            <div style="position:absolute;left:58.20px;top:537.70px" class="cls_003">
                <span class="cls_003">16. Do you belong to any minority community? Yes / No. If yes, state the following:</span>
            </div>
            <div style="position:absolute;left:58.20px;top:551.70px" class="cls_003">
                <span class="cls_003">Community:</span>
                <span class="cls_008">................................................................................</span>
                <span class="cls_003">. Religion:</span>
                <span class="cls_008">................................................................................</span>
            </div>
            <div style="position:absolute;left:58.20px;top:565.80px" class="cls_003">
                <span class="cls_003">Note: the Information at item 14 & 15 is for statistical purpose only.</span>
            </div>
            <div style="position:absolute;left:58.20px;top:579.80px" class="cls_003">
                <span class="cls_003">17. Do you require hostel accommodation : Yes / No</span>
            </div>
            <div style="position:absolute;left:58.20px;top:593.90px" class="cls_003">
                <span class="cls_003">18. Are you interested to enroll in NSS : Yes / No</span>
            </div>
        </div>
        </body>

        </html>