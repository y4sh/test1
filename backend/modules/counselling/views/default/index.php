<?php
    $this->title = 'CUCET Upload Data';

    use yii\web\View;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use common\assets\JqGridAsset;
    use common\assets\IntroAsset;
    $this->registerCss("
        .positive-row { background-color: #7BF27B;}
        .negative-row { background-color: #FFA6A6;}      
    ");

    $this->registerJsFile('@web/custom/js/counselling_cucet_upload.js', ['depends' => [JqGridAsset::className()]]);
?>
<div class="col-xs-18 col-sm-12 col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">Upload CUCET Result</div>
        <div class="panel-body">
            <?php
            $form = ActiveForm::begin([
                'id' => 'upload-excel-form',
                'options' => ['enctype' => 'multipart/form-data'],
                'enableClientValidation'=> true,
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => true,
                'action' => 'upload_excel_sheet'
            ]);
                echo '<select name="program_id" id="program_id" class="form-control">';
                    echo $programOpt;
                echo '</select><br><br>';
                echo $form->field($UploadExcelData, 'excel_file')->fileInput();
                echo Html::submitButton('Upload', ['class' => 'btn btn-lg btn-success btn-block']);
            ActiveForm::end();
            ?>
        </div>
    </div>
</div>
<br>
<div class="col-xs-18 col-sm-12 col-md-12">
    <table id="excel_uploaded_grid"></table>
    <div id="pager_excel_uploaded_grid"></div>
</div>

<div id="overlay-loading"></div>
<div id="loading-image">
    <img src="<?= Yii::$app->homeUrl . "custom/images/loading.gif" ?>" />
</div>