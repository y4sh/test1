<?php

namespace app\modules\settings\controllers;

use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * Default controller for the `settings` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','get_instructers'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }	
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionGet_instructers(){
    	$query = (new \yii\db\Query())
                    ->select("*")
                    ->from(['cucet_result'])                    
                    ->all();  
        return $this->asJson($query); 
    }
    public function actionSave_inst(){
		if (Yii::$app->request->post()) {
			$oper		=	$_POST['oper'];				
			switch ($oper) {
			    case "add":			    				        
			        $inst_name	=	$_POST['instructername'];
					$dept_name 	=	$_POST['dept_name'];
					Yii::$app->db->createCommand()->insert('instructers', [
					    'instructername' => $inst_name,
					    'dept_name' => $dept_name,
					])->execute();
			    break;
			    case "edit":
			        $inst_name	=	$_POST['instructername'];
					$dept_name 	=	$_POST['dept_name'];
					$id 		=	$_POST['id'];
					Yii::$app->db->createCommand("UPDATE instructers SET instructername=:inst_name, dept_name=:dept_name WHERE instructerid=:id")
					->bindValue(':id', $id)
					->bindValue(':inst_name', $inst_name)
					->bindValue(':dept_name', $dept_name)
					->execute();
					/*Yii::$app->db->createCommand()->update('instructes', ['instructername' => 1,'dept_name'=>$dept_name], 'age > 30')->execute();*/
			    break;
			    case "del":
			    	$id 	=	$_POST['id'];
			        Yii::$app->db->createCommand()->delete('instructers', ['instructerid'=>$id])->execute();
			    break;			   
			}
		}
		else return "error in post data";
    }
}
