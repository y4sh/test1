$( document ).ready(function() { 
    $("#jsGrid").jsGrid({
        height: "auto",
        width: "100%",
 
        sorting: true,
        paging: false,
        autoload: true,
 
        controller: {
            loadData: function() {
                var d = $.Deferred();
 
                $.ajax({
                    url: "http://cas-frontend.test/settings/default/json",
                    dataType: "json"
                }).done(function(data, statusText, resObject) {
                 
                    })

                .done(function(response) {
                    d.resolve(response.value);
                });                
                return d.promise();
            }
        },
          
        fields: [
            { name: "courseid", type: "text" },
            { name: "coursename", type: "textarea", width: 150 },
            { name: "coursestatus", type: "number", width: 50, align: "center",
              name: "campusname", type: "number", width: 50,
            }
        ]
    });
});