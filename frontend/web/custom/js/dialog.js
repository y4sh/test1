$('#document').ready(function(){
	$('.openDlg').live('click', function(){
		var dialogId = $(this).attr('class').replace('openDlg ', "");
		$.ajax({
			'type': 'POST',
			'url' : $(this).attr('href'),
			success: function (data) {
				$('#'+dialogId+' div.divForForm').html(data);
				$( '#'+dialogId ).dialog( 'open' );
			},
			dataType: 'html'
		});
		return false; // prevent normal submit
	})
});
</script>