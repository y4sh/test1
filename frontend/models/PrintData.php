<?php


namespace frontend\models;

use Yii;
use yii\base\Model;


use yii\db\ActiveRecord;

class PrintData extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'courses';
    }
}