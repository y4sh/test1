<?php

namespace app\modules\settings\controllers;

use yii\web\Controller;

/**
 * Default controller for the `settings` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionJson()
    {
		$query = (new \yii\db\Query())
			->select("*")
			->from(['courses'])                
			->all();
		//return print_r($query);
		$response['allcourse'] = $query;
        /*\yii\helpers\Json::htmlEncode([$query]);  
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        */
        $response = \Yii::$app->response;
		$response->format = \yii\web\Response::FORMAT_JSON;
		$response->data =$query;
        return $response->data;
    }
    
}
