<?php
$this->title = "Settings Home Page";
$this->registerJsFile(
    '@web/custom/js/jsgrd.js',
    ['depends' => [\common\assets\JsgridAsset::className()]]
);
?>
<table id="jsGrid"></table>
<div id="jsGrid"></div>