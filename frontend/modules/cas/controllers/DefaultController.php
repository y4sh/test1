<?php

namespace app\modules\cas\controllers;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use app\modules\cas\models\Department;
use app\modules\cas\models\Courses;
use app\modules\cas\models\Studentlogin;
use app\modules\cas\models\Session;
use app\modules\cas\models\Semester;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\cas\models\FeedbackData;



/**
 * Default controller for the `cas` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['selectcss','searchcss','cas','feed'],
                'rules' => [                
                    [
                        'actions' => ['searchcss','cas','selectcss','feed'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],           
        ];
    }




    public function actionIndex()
    {        
    $ses=Yii::$app->session;
    if($ses->isActive){
            try{
                $enrollment_id=Yii::$app->user->identity->username;
                $ses->set('enrollment_id', $enrollment_id);        
                $course_id = (new \yii\db\Query())
                    ->select("course_id")
                    ->from(['Student_course'])                
                    ->where(['student_id'=>$enrollment_id])
                    ->one();
                $ses->set('course_id', $course_id['course_id']); 
                //Finding All Courses from Subjects2 Table
                $query = (new \yii\db\Query())
                    ->select("*")
                    ->from(['subjects'])
                    ->where(['coursename'=>$course_id['course_id']])
                    ->all();

                //return $this->redirect(array('/cas/default/selectcss'));
                

                //return print_r($query);

                //submit data of all feedback
                $feed = new FeedbackData(); 
                $request    = Yii::$app->request;               
                //$lecture_preparedness = $request->post('lecture_preparedness');
                if ($feed->load(Yii::$app->request->post())) {
                    //$feed->instructer = $request->post('FeedbackData')['lecture_preparedness'];
                    $subject_code = $request->post('FeedbackData')['subject_code'];
                    //$feed->program = $request->post('FeedbackData')['program'];/*
                   /* 
                    $feed->subjectmatter_knowledge = $request->post('FeedbackData')['subjectmatter_knowledge'];
                    $feed->lecture_preparedness = $request->post('FeedbackData')['lecture_preparedness'];
                    $feed->abilityto_inspire = $request->post('FeedbackData')['abilityto_inspire'];
                    $feed->coursematerial_selection = $request->post('FeedbackData')['coursematerial_selection'];
                    $feed->presentationand_teachingstyle = $request->post('FeedbackData')['presentationand_teachingstyle'];
                    $feed->abilitytodraw_interest = $request->post('FeedbackData')['abilitytodraw_interest'];
                    $feed->availableto_help = $request->post('FeedbackData')['availableto_help'];
                    $feed->classtime_utilisation = $request->post('FeedbackData')['classtime_utilisation'];
                    $feed->assignmentsand_tests = $request->post('FeedbackData')['assignmentsand_tests'];
                    $feed->evaluationand_grading = $request->post('FeedbackData')['evaluationand_grading'];
                    $feed->test_quality = $request->post('FeedbackData')['test_quality'];
                    $feed->reading_relavance = $request->post('FeedbackData')['reading_relavance'];
                    $feed->complementrystudy_materials = $request->post('FeedbackData')['complementrystudy_materials'];
                    $feed->learningand_understanding = $request->post('FeedbackData')['learningand_understanding'];
                    $feed->self_efforts = $request->post('FeedbackData')['self_efforts'];
                    $feed->expected_grade = $request->post('FeedbackData')['expected_grade'];
                    $feed->suggested_improvements = $request->post('FeedbackData')['suggested_improvements'];
                    */
                    $exists=$feed::find()->where(['subject_code'=>$subject_code,'user_name'=>$enrollment_id])->exists();*/
                    if($exists){
                        return $this->render('feedback',['result'=>$query]);

                        $feed->self_efforts = $request->post('FeedbackData')['self_efforts'];
                        $feed->self_efforts = $request->post('FeedbackData')[''];
                    }
                    else{
                        $feed->save(false);                    
                    }
                }
                return $this->render('feedback',['result'=>$query]);
                //return print_r($request->post('FeedbackData')['lecture_preparedness']);
            }
            catch (Exception $exception){
                return "jljlkjlk";
            }      
          
        }
    }
    
    public function actionFeed(){
        return $this->render('feed');
    }
    public function actionSubmit(){
    	$form_model = new Department();	
    	$request    = Yii::$app->request; 
    	$params = $request->bodyParams;
    	$dept = $request->post('dept_name');
    	if ($form_model->load(Yii::$app->request->post())) {
            $form_model->dept_name = $request->post('Department')['dept_name'];
            $form_model->save();            
        }
        
    	return $this->render('index',[
            'form_model' => $form_model
        ]);    	 
    }
    public function actionSubcourse(){
        $form_model2=new Courses;
        // $request=Yii::$app->request;
        $coursename=$request->post('coursename');
        if($form_model2->load(Yii::$app->request->post())){
            $form_model2->coursename=$request->post('Courses')['coursename'];
            $form_model2->campusname=$request->post('Courses')['campusname'];
            $form_model2->coursestatus=$request->post('Courses')['coursestatus'];
            $form_model2->save();
        return print_r($request->post('Courses'));
        /*return $this->render('courses',[
            'form_model' => $form_model2
            ]);*/
        }
    }
    public function actionUpdatecourse()
    {   $form_model3=new Courses;
        $model = $form_model3->findModel(4);

        $model->coursename="updatebycode";
        $model->save();
        return print_r($model->coursename);
    }
    public function actionSelectcss()
    {
        //return $this->render('selectcss');
        $form_course=new Courses();
        $form_dept=new Department();
        $form_session=new Session();
        $form_sem= new Semester();
        $course = ArrayHelper::map($form_course::find()->where(['coursestatus' => "1"])->all(), 'coursename', 'coursename');
        $dept = ArrayHelper::map($form_dept::find()->all(), 'dept_name', 'dept_name');
        $session = ArrayHelper::map($form_session::find()->all(), 'sessionname', 'sessionname');
        $sem=ArrayHelper::map($form_sem::find()->all(), 'semestername', 'semestername');
      

        return $this->render('selectcss',['model'=>$form_course,'model'=>$form_sem, 'model'=>$form_session,'course'=>$course,'sem'=>$sem,'sessionid'=>$session]);   
        //return print_r($items);
    }
    public function actionSearchcss()
    {
        
        $request=Yii::$app->request;
        if($request->post())
        {
            $postdata=$request->post();
            $dept_name=$request->post('Department')['dept_name'];
            $session=$request->post('Session')['sessionid'];
            $semester=$request->post('Semester')['semesterid'];
            $course=$request->post('Courses')['coursename'];

            //$sem_name=$request->post('Department')['semestername'];
            
            $query = (new \yii\db\Query())
                ->select("*")
                ->from(['subjects2'])                
                ->where(['coursename'=>$course,'semestername'=>$semester, 'sessionname' => $session])
                ->all();
            //return print_r($query);
            //return print_r($query);
            return $this->render('feedback',['result'=>$query,'semester'=>$semester,'session'=>$session,'course'=>$course]);
        }
        else{
            return $this->redirect(array('/cas/default/selectcss', '#'=>'access_direct'));
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); 
        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            //prepare model to save if necessary
            $model->save();
            return $this->redirect(['index']); //<---This would return to the gridview once model is saved
        }
        return $this->renderAjax('index', [
            'model' => $model,
        ]);
    }
    
}
