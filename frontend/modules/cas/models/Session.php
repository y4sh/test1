<?php

namespace app\modules\cas\models;

use Yii;

class Session extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sessionname'], 'required'],
            [['sessionstatus'], 'string'],
            [['sessionname'], 'string', 'max' => 255],
            [['sessionname'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sessionid' => Yii::t('app', 'Sessionid'),
            'sessionname' => Yii::t('app', 'Sessionname'),
            'sessionstatus' => Yii::t('app', 'Sessionstatus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemesters()
    {
        return $this->hasMany(Semester::className(), ['sessionid' => 'sessionid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subjects::className(), ['sessionname' => 'sessionname']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects2s()
    {
        return $this->hasMany(Subjects2::className(), ['sessionname' => 'sessionname']);
    }
}
