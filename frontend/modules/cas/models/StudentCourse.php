<?php

namespace app\modules\cas\models;

use Yii;

/**
 * This is the model class for table "Student_course".
 *
 * @property int $id
 * @property string $course_id
 * @property string $student_id
 * @property string $Allow
 */
class StudentCourse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Student_course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Allow'], 'required'],
            [['Allow'], 'string'],
            [['course_id', 'student_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'student_id' => Yii::t('app', 'Student ID'),
            'Allow' => Yii::t('app', 'Allow'),
        ];
    }
}
