<?php

namespace app\modules\cas\models;

use Yii;

class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coursename', 'campusname'], 'required'],
            [['coursestatus'], 'string'],
            [['coursename'], 'string', 'max' => 255],
            [['campusname'], 'string', 'max' => 20],
            [['coursename'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'courseid' => Yii::t('app', 'Courseid'),
            'coursename' => Yii::t('app', 'Coursename'),
            'coursestatus' => Yii::t('app', 'Coursestatus'),
            'campusname' => Yii::t('app', 'Campusname'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['programme' => 'coursename']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects0()
    {
        return $this->hasMany(Subjects::className(), ['coursename' => 'coursename']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects2s()
    {
        return $this->hasMany(Subjects2::className(), ['coursename' => 'coursename']);
    }
    public function findModel($courseid)
    {   
        $courseid=4;
        if (($model = Courses::findOne(['courseid' => $courseid])) !== null) {
            return $model;
        }
    }
    public function actionUpdatecourse()
    {
        $model = $this->findModel(4);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }*/

        return print_r($model);
    }

}
