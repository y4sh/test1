<?php

namespace app\modules\cas\models;

use Yii;

/**
 * This is the model class for table "semester".
 *
 * @property int $semesterid
 * @property string $semestername
 * @property int $sessionid
 * @property string $semesterstatus
 * @property string $sessionname
 *
 * @property Session $session
 * @property Subject[] $subjects
 * @property Subjects[] $subjects0
 * @property Subjects2[] $subjects2s
 */
class Semester extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semester';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semestername', 'sessionid', 'sessionname'], 'required'],
            [['sessionid'], 'integer'],
            [['semesterstatus'], 'string'],
            [['semestername'], 'string', 'max' => 255],
            [['sessionname'], 'string', 'max' => 20],
            [['semestername'], 'unique'],
            [['sessionid'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['sessionid' => 'sessionid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'semesterid' => Yii::t('app', 'Semesterid'),
            'semestername' => Yii::t('app', 'Semestername'),
            'sessionid' => Yii::t('app', 'Sessionid'),
            'semesterstatus' => Yii::t('app', 'Semesterstatus'),
            'sessionname' => Yii::t('app', 'Sessionname'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['sessionid' => 'sessionid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['semestername' => 'semestername']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects0()
    {
        return $this->hasMany(Subjects::className(), ['semestername' => 'semestername']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects2s()
    {
        return $this->hasMany(Subjects2::className(), ['semestername' => 'semestername']);
    }
}
