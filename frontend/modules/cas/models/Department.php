<?php

namespace app\modules\cas\models;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $dept_name
 *
 * @property Instructers[] $instructers
 * @property Subjects[] $subjects
 * @property Subjects2[] $subjects2s
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dept_name'], 'required'],
            [['dept_name'], 'string', 'max' => 50],
            [['dept_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dept_name' => Yii::t('app', 'Department Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstructers()
    {
        return $this->hasMany(Instructers::className(), ['dept_name' => 'dept_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subjects::className(), ['ins_dept' => 'dept_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects2s()
    {
        return $this->hasMany(Subjects2::className(), ['ins_dept' => 'dept_name']);
    }

    public function submitdept()
    {
             
        $dept = new Department();
        $dept->dept_name ="test dept";        
        
        return $dept->save();
    }
}
