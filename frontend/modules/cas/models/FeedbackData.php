<?php

namespace app\modules\cas\models;

use Yii;
class FeedbackData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subjectname', 'semestername', 'subject_code', 'program', 'subjectmatter_knowledge', 'lecture_preparedness', 'abilityto_inspire', 'coursematerial_selection', 'presentationand_teachingstyle', 'abilitytodraw_interest', 'availableto_help', 'classtime_utilisation', 'assignmentsand_tests', 'evaluationand_grading', 'test_quality', 'reading_relavance', 'complementrystudy_materials', 'learningand_understanding', 'course_rating', 'self_efforts', 'expected_grade', 'suggested_improvements'], 'required'],
            [['subjectmatter_knowledge', 'lecture_preparedness', 'abilityto_inspire', 'coursematerial_selection', 'presentationand_teachingstyle', 'abilitytodraw_interest', 'availableto_help', 'classtime_utilisation', 'assignmentsand_tests', 'evaluationand_grading', 'test_quality', 'reading_relavance', 'complementrystudy_materials', 'learningand_understanding', 'course_rating'], 'integer'],
            [['user_name'], 'string', 'max' => 20],
            [['subjectname', 'instructer', 'semestername', 'session', 'campus'], 'string', 'max' => 255],
            [['subject_code'], 'string', 'max' => 50],
            [['program'], 'string', 'max' => 30],
            [['self_efforts'], 'string', 'max' => 10],
            [['expected_grade'], 'string', 'max' => 2],
            [['suggested_improvements'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'feedbackid' => Yii::t('app', 'Feedbackid'),
            'user_name' => Yii::t('app', 'User Name'),
            'subjectname' => Yii::t('app', 'Subjectname'),
            'instructer' => Yii::t('app', 'Instructer'),
            'semestername' => Yii::t('app', 'Semestername'),
            'subject_code' => Yii::t('app', 'Subject Code'),
            'program' => Yii::t('app', 'Program'),
            'session' => Yii::t('app', 'Session'),
            'campus' => Yii::t('app', 'Campus'),
            'subjectmatter_knowledge' => Yii::t('app', 'Subjectmatter Knowledge'),
            'lecture_preparedness' => Yii::t('app', 'Lecture Preparedness'),
            'abilityto_inspire' => Yii::t('app', 'Abilityto Inspire'),
            'coursematerial_selection' => Yii::t('app', 'Coursematerial Selection'),
            'presentationand_teachingstyle' => Yii::t('app', 'Presentationand Teachingstyle'),
            'abilitytodraw_interest' => Yii::t('app', 'Abilitytodraw Interest'),
            'availableto_help' => Yii::t('app', 'Availableto Help'),
            'classtime_utilisation' => Yii::t('app', 'Classtime Utilisation'),
            'assignmentsand_tests' => Yii::t('app', 'Assignmentsand Tests'),
            'evaluationand_grading' => Yii::t('app', 'Evaluationand Grading'),
            'test_quality' => Yii::t('app', 'Test Quality'),
            'reading_relavance' => Yii::t('app', 'Reading Relavance'),
            'complementrystudy_materials' => Yii::t('app', 'Complementrystudy Materials'),
            'learningand_understanding' => Yii::t('app', 'Learningand Understanding'),
            'course_rating' => Yii::t('app', 'Course Rating'),
            'self_efforts' => Yii::t('app', 'Self Efforts'),
            'expected_grade' => Yii::t('app', 'Expected Grade'),
            'suggested_improvements' => Yii::t('app', 'Suggested Improvements'),
        ];
    }
}
