<?php

namespace app\modules\cas\models;

use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property string $sub_code
 * @property string $sub_name
 * @property string $programme
 * @property string $semestername
 *
 * @property Semester $semestername0
 * @property Courses $programme0
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_code', 'sub_name', 'programme', 'semestername'], 'required'],
            [['sub_code'], 'string', 'max' => 8],
            [['sub_name', 'programme'], 'string', 'max' => 100],
            [['semestername'], 'string', 'max' => 10],
            [['sub_code', 'sub_name', 'programme'], 'unique', 'targetAttribute' => ['sub_code', 'sub_name', 'programme']],
            [['semestername'], 'exist', 'skipOnError' => true, 'targetClass' => Semester::className(), 'targetAttribute' => ['semestername' => 'semestername']],
            [['programme'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['programme' => 'coursename']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_code' => Yii::t('app', 'Sub Code'),
            'sub_name' => Yii::t('app', 'Sub Name'),
            'programme' => Yii::t('app', 'Programme'),
            'semestername' => Yii::t('app', 'Semestername'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemestername0()
    {
        return $this->hasOne(Semester::className(), ['semestername' => 'semestername']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramme0()
    {
        return $this->hasOne(Courses::className(), ['coursename' => 'programme']);
    }
}
