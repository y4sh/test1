<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\modules\cas\models\FeedbackData;
use yii\web\Session;
use yii\helpers\Url;
$model=new FeedbackData();
?>
<div class="feedback">
	<?php 
		$request    = Yii::$app->request;    	
    	$cname 		= $request->get('cname');
    	$subcode 	= $request->get('subcode');
    	$inst 		= $request->get('ins');   	
    	//print $subcode;
    	//print $inst;
    ?>

	<table class="table table-bordered">
        <tr class='danger'>
            <th><?php  echo "   ".$cname    ?></th>
            <th><?php  echo "   ".$subcode ?></th>
            <th><?php  echo "   ".$inst ?></th>
        </tr>
        <tr>
            <th>Subject Name:</th>
            <th>Semester:</th>
            <th>Session :</th>
            
        </tr>    
   </table>
	<?php 
       
        $ses=Yii::$app->session;
        if($ses->isActive){
            try{
                $enrollment_id=Yii::$app->user->identity->username;
                $ses->set('enrollment_id', $enrollment_id);                            
            }
            catch (Exception $exception){
                print $exception;
            }
        }        
        //print $ses['enrollment_id'];
    ?>

	
    <?php $form = ActiveForm::begin(['action' => '/cas']); ?>

        <?= $form->field($model, 'subjectmatter_knowledge')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'lecture_preparedness')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'abilityto_inspire')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'coursematerial_selection')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'presentationand_teachingstyle')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'abilitytodraw_interest')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'availableto_help')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'classtime_utilisation')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'assignmentsand_tests')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'evaluationand_grading')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'test_quality')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'reading_relavance')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'complementrystudy_materials')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'learningand_understanding')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'course_rating')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'self_efforts')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'expected_grade')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>
        <?= $form->field($model, 'suggested_improvements')->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']) ?>

        <?= $form->field($model, 'user_name')->hiddenInput(['value'=>$ses['enrollment_id']])->label(false);?>
        <?= $form->field($model, 'instructer')->hiddenInput(['value'=>$inst])->label(false);?>
        <?= $form->field($model, 'session')->hiddenInput(['value'=>'test'])->label(false);?>
        <?= $form->field($model, 'subject_code')->hiddenInput(['value'=>$subcode])->label(false); ?>


        
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- feedback -->
A Product of Yii Software LLC