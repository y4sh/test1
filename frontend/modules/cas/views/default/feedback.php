<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\modules\cas\models\FeedbackData;
use yii\web\Session;
use yii\helpers\Url;
$model=new FeedbackData();
?>

<div class="row" style="margin-top:3%" >
   <?php 
       
        $ses=Yii::$app->session;
        if($ses->isActive){
            try{
                $enrollment_id=Yii::$app->user->identity->username;
                $ses->set('enrollment_id', $enrollment_id);                            
            }
            catch (Exception $exception){
                print $exception;
            }
        }        
        //print $ses['enrollment_id'];

   ?>
</div>
   <table class="table table-bordered">
        <tr class='danger'>
            <th>Course  :1</th>
            <th>Semester:2</th>
            <th>Session :3</th>
            
        </tr>    
   </table>

    <table class="table table-bordered">
    <thead>
      <tr class='success'>
        <th>Sub. Code</th>
        <th>Course Title</th>
        <th>Instructor</th>
        <th>For Feedback</th>
      </tr>
    </thead>
    <tbody>
        <?php 
            try {           
	                foreach ($result as $rows) 
	                {   
	                    $cname = $rows['coursename'];
	                    $subcode = $rows['subjectcode'];
	                    $ins = $rows['instructername'];
	                    print "<tr class='warning'><td>".$subcode."</td><td>".$cname."</td><td>".$ins."</td><td>";     
                        //Check if Already Submitted                   
                        $test=Yii::$app->db->createCommand('select user_name from feedback_data WHERE user_name=:user_name AND subject_code=:subcode')
                            ->bindValue(':user_name',$enrollment_id)
                            ->bindValue(':subcode',$subcode)
                            ->queryOne();                        
                        if(empty($test)){
                            echo Html::a('submit', Url::to(['default/feed','cname'=>$cname,'subcode'=>$subcode,'ins'=>$ins]), ['class' => 'btn btn-primary']);
                        }
                        else{
                            echo Html::a('Done', [''],['class' => 'btn btn-success']);                             
                        }

	                    /*echo Html::submitButton(Yii::t('app', 'Click here'), ['class' => 'btn btn-primary click_submit','id'=>'','value'=>$subcode]) ;*/
                        
	                    print "</td></tr>";
	                }
                }
            catch (Exception $e){
                echo $e;
            }           
        ?> 
    </tbody>
  </table>     
    </tbody></table>

</div>
</div>
