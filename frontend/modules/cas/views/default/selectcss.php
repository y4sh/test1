<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cas\models\Courses;
use app\modules\cas\models\Department;
use app\modules\cas\models\Session;
use app\modules\cas\models\Semester;
$model=new Courses();
$model3=new Session();
$model4=new Semester()
/* @var $this yii\web\View */
/* @var $model app\modules\cas\models\Studentlogin */
/* @var $form ActiveForm */

?>
<div class="selectcss">
    <?php $form = ActiveForm::begin(['action' => '/cas/default/searchcss']);?>
    <div>
       <h1 class="text-center">Please Select .......... </h1>
    </div>    

    <div class="row" style="margin-top:4%" ></div>

    <div class="row" style="margin-bottom:10px">
       <label for="" class="col-md-2 col-md-offset-3 control-label">
          <h4>Programme</h4>
       </label>
       <div class="col-md-4"  ><?= Html::activeDropDownList($model, 'coursename',$course,['class'=>'form-control input-lg'])?></div>
    </div>

    <div class="row" style="margin-bottom:10px">
       <label for="" class="col-md-2 col-md-offset-3 control-label">
          <h4>Session</h4>
       </label>
       <div class="col-md-4">
          <?= Html::activeDropDownList($model3, 'sessionid',$sessionid,['class' => 'form-control input-lg '])?>  
       </div>
       <div class="col-md-4" >
       </div>
    </div>

    <div class="row" style="margin-bottom:10px">
       <label for="" class="col-md-2 col-md-offset-3 control-label">
          <h4>Semester</h4>
       </label>
       <div class="col-md-4 "> <?= Html::activeDropDownList($model4, 'semesterid',$sem,['class' => 'form-control input-lg '])?> </div>
    </div>

    <div class="form-group">
       <div class="row" style="margin-top:25px">
          <div class="col-md-2 col-md-offset-5"> <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?></div>       
       </div>
    </div>
    <?php ActiveForm::end(); ?>
</div><!-- selectcss -->
