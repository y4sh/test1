<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\modules\cas\models\FeedbackData;
$model=new FeedbackData();
$this->registerJsFile('@web/custom/js/model-test.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="row" style="margin-top:3%" ></div>
   <table class="table table-bordered">
        <tr class='danger'>
            <th>Course  :<?php print "   ".$course;?></th>
            <th>Semester:<?php print "   ".$semester;?></th>
            <th>Session :<?php print "   ".$session;?></th>
            
        </tr>    
   </table>

    <table class="table table-bordered">
    <thead>
      <tr class='success'>
        <th>Sub. Code</th>
        <th>Course Title</th>
        <th>Instructor</th>
        <th>For Feedback</th>
      </tr>
    </thead>
    <tbody>
       <?php 
            try {           
	                foreach ($result as $rows) 
	                {   
	                    $ins = $rows['instructername'];
	                    $subcode = $rows['subjectcode'];
	                    $subjectname = $rows['subjectname'];
	                    print "<tr class='warning'><td>".$subcode."</td><td>".$subjectname."</td><td>".$ins."</td><td>";
	                    echo Html::submitButton(Yii::t('app', 'Click here'), ['class' => 'btn btn-primary click_submit','id'=>'','value'=>$subcode]) ;
	                    print "</td></tr>";
	                }
                }
            catch (Exception $e){
                echo $e;
            }           
        ?>
    </tbody>
  </table> 
<?php
    Modal::begin([
        'header'=>'<h4>Please Fill.......</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<table class='table table-bordered'>
    <thead>
      
    </thead>
    <tbody>";


    ?>
    <?php $form = ActiveForm::begin(); ?>

        <tr> 
            <td><?= $form->field($model, 'subjectname')->label('Subjects') ?></td>
            <td> <?= $form->field($model, 'semestername')->label('Semester') ?> </td>
            <td><?= $form->field($model, 'subject_code')->label('Subject Code') ?> </td>
            <td><?= $form->field($model, 'program')->label('Course') ?> </td>
        </tr>
        <tr> <td><?= $form->field($model, 'subjectmatter_knowledge')->radioList(array('1'=>'One',2=>'Two')); ?> </td><tr>
        <tr> <td><?= $form->field($model, 'lecture_preparedness') ?> </td></tr>
        <tr> <td><?= $form->field($model, 'abilityto_inspire') ?> </td></tr>
        <tr> <td><?= $form->field($model, 'coursematerial_selection') ?> </td></tr>
        <tr> <td><?= $form->field($model, 'presentationand_teachingstyle') ?> </td></tr>
        <tr> <td><?= $form->field($model, 'abilitytodraw_interest') ?></tr>
        <tr> <td><?= $form->field($model, 'availableto_help') ?> </td></tr>
        <tr> <td><?= $form->field($model, 'classtime_utilisation') ?></td></tr>
        <tr> <td><?= $form->field($model, 'assignmentsand_tests') ?></td></tr>
        <tr> <td><?= $form->field($model, 'evaluationand_grading') ?></td></tr>
        <tr> <td><?= $form->field($model, 'test_quality') ?></td></tr>
        <tr> <td><?= $form->field($model, 'reading_relavance') ?></td></tr>
        <tr> <td><?= $form->field($model, 'complementrystudy_materials') ?></td></tr>
        <tr> <td><?= $form->field($model, 'learningand_understanding') ?></td></tr>
        <tr> <td><?= $form->field($model, 'course_rating') ?></td></tr>
        <tr> <td><?= $form->field($model, 'self_efforts') ?></td></tr>
        <tr> <td><?= $form->field($model, 'expected_grade') ?></td></tr>
        <tr> <td><?= $form->field($model, 'suggested_improvements') ?></td></tr>
        <tr> <td><?= $form->field($model, 'user_name') ?></td></tr>
        <tr> <td><?= $form->field($model, 'instructer') ?></td></tr>
        <tr> <td><?= $form->field($model, 'session') ?></td></tr>
        <tr> <td><?= $form->field($model, 'campus') ?></td></tr>
    
      
          <tr>  <td><?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?></td></tr>
      
    <?php ActiveForm::end(); ?>
    </tbody></table>
   <?php  Modal::end();

?>


</div>
</div>