<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cas\models\Courses;
$model=new Courses();
/* @var $this yii\web\View */
/* @var $model app\modules\cas\models\Courses */
/* @var $form ActiveForm */
?>
<div class="courses">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'coursename') ?>
        <?= $form->field($model, 'campusname') ?>
        <?= $form->field($model, 'coursestatus') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- courses -->
