<?php
$this->title = 'Your Courses';

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\assets\JqGridAsset;
$this->registerCss("
    form .form-control {
        font-size:11px !important;
    }

    form {
        font-size:11px !important;
    }"
);
$this->registerJs(
    "var userId = " . $userId . '; var studentId = ' . $studentId,
    View::POS_HEAD,
    'my-varibles'
);
$this->registerJsFile('@web/custom/js/student_feedback.js', ['depends' => [JqGridAsset::className()]]);
?>
<div class="col-xs-18 col-sm-12 col-md-12">
    <table id="getfeedback"></table>
    <div id="pager_getfeedback"></div><br/>
</div>
<div class="col-xs-18 col-sm-12 col-md-12">
    <table id="feedback"></table>
    <div id="pager_feedback"></div><br/>
</div>

<!-- Modal -->
<div class="modal fade" id="myFeedbackModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myFeedbackAddModalLabel"><!-- Provide Course Feedback <small>Please select the number corresponding to your rating.</small> --></h4>
            </div>
            <div class="modal-body">
                <p>
                Assessment of a course is an important task. As a student , one must be fair and objective in assessing each item of this
                questionnaire on a five point scale.<br>
                <span class="text-success">
                    <i><strong>
                        5. Very Good,<br>
                        4. Good,<br>
                        3. Average,<br>
                        2. Fair,<br>
                        1. Poor,<br/>
                    </i></strong>
                </span>
                </p>
                <hr>
				<?php
                $form = ActiveForm::begin([
                    'id' => 'feedback-form',
                    // 'options' => [
                    //     'autocomplete' => 'off',
                    // ],
                    'enableClientValidation'=> true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'action' => 'save_batch_courses_feedback_now',
                    // 'layout' => 'horizontal',
                    // 'fieldConfig' => [
                        // 'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        /*'horizontalCssClasses' => [
                            'label' => 'col-sm-8',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => ''
                        ]*/
                    // ]
                ]);
                	echo '<div class="col-md-6">'; 
					    echo $form->field($StudentFeedbackData, 'subjectmatter_knowledge')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'lecture_preparedness')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'abilityto_inspire')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'coursematerial_selection')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'presentationand_teachingstyle')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'abilitytodraw_interest')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'availableto_help')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'classtime_utilisation')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'assignmentsand_tests')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
                        echo $form->field($StudentFeedbackData, 'evaluationand_grading')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
                    echo '</div>';
                    echo '<div class="col-md-6">'; 
					    echo $form->field($StudentFeedbackData, 'test_quality')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'reading_relavance')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'complementrystudy_materials')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'learningand_understanding')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'course_rating')->inline(true)->radioList([1 => '1' , 2=>'2', 3=>'3', 4=>'4' , 5=>'5']);
					    echo $form->field($StudentFeedbackData, 'self_efforts')->dropDownList(['Lots' => 'Lots' , "Average"=>'Average', "Little"=>'Little'], ['prompt'=>'Choose From List ...']);
					    echo $form->field($StudentFeedbackData, 'expected_grade')->dropDownList(["O"=>"O", "A+" => 'A+' ,"A" => 'A' , "B+"=>'B+', "B"=>'B', "C"=>'C' , "P"=>'P', "F"=>'F'], ['prompt'=>'Choose From List ...']);
					    echo $form->field($StudentFeedbackData, 'suggested_improvements')->textarea(['rows' => '6','placeholder' => 'Suggested Improvements']);
				    echo '</div>';
                    echo '<div style="display:none">';
                    echo $form->field($StudentFeedbackData, 'id')->hiddenInput(['value' => 0])->label(false);
                    echo $form->field($StudentFeedbackData, 'user_id')->hiddenInput(['value' => $userId])->label(false);
                    echo $form->field($StudentFeedbackData, 'student_id')->hiddenInput(['value' => $studentId])->label(false);
                    echo $form->field($StudentFeedbackData, 'student_course_id')->hiddenInput(['value' => ''])->label(false);
                    echo '</div>';

			    ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="col-md-6">
                    <button type="button" class="btn btn-lg btn-danger btn-block pull-left" data-dismiss="modal">Close</button>
                </div>
                <div class="col-md-6">
                    <?= Html::submitButton('Save', ['id' => 'save-Feedback', 'class' => 'btn btn-lg btn-success btn-block pull-right']) ?>
                </div>
            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div id="overlay-loading"></div>
<div id="loading-image">
    <img src="<?= Yii::$app->homeUrl . "custom/images/loading.gif" ?>" />
</div>