fsa<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\HtmlPurifier;
use yii\filters\AccessControl;
use app\modules\course\models\StudentFeedbackData;

class DefaultController extends Controller
{
	public function behaviors()
	{
	    return [
	        'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [
	                [
	                    'actions' => [
                            'feedback',
                            'get_batch_courses_feedback_now', 'get_batch_courses_feedback', 'save_batch_courses_feedback_now'
	                    ],
	                    'allow' => true,
	                    'roles' => ["@"]
	                ]
	            ]
	        ]
	    ];
	}	

    public function actionFeedback()
    {
        $StudentFeedbackData = new StudentFeedbackData();
        $userId = Yii::$app->user->id;
        $studentId = Yii::$app->db->createCommand("SELECT id FROM student_basic WHERE userId=:userId")->bindValue(":userId", $userId)->queryScalar();

        return $this->render('feedback', [
            'StudentFeedbackData' => $StudentFeedbackData,
            'userId' => $userId,
            'studentId' => $studentId
        ]);
    }
    public function actionGet_batch_courses_feedback_now()
    {
        $userId = Yii::$app->user->identity->id;

        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);

        if(!$sidx) $sidx = "course_code";
        if(!$sord) $sord = "ASC";

        // $batch_id = $_POST['batch_id'] *1;
        $count = Yii::$app->db->createCommand(
            "SELECT
                COUNT(*)
            FROM
                adm_batch_current_courses AS abcc
            LEFT JOIN student_courses AS sc ON sc.batch_course_id=abcc.id
            LEFT JOIN student_basic AS sb ON sb.id=sc.student_id
            WHERE
                abcc.feedback IS NOT NULL AND sc.feedback_done IS NULL AND sb.userId=:userId"
            )
            ->bindValue(":userId", $userId)
            ->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)

        if($start < 0) $start = 0;
        
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand(
                "SELECT abcc.*, u.username, u.email
                FROM adm_batch_current_courses AS abcc
                LEFT JOIN user AS u ON u.id=abcc.teacher_id
                LEFT JOIN student_courses AS sc ON sc.batch_course_id=abcc.id
                LEFT JOIN student_basic AS sb ON sb.id=sc.student_id
                WHERE abcc.feedback IS NOT NULL AND sc.feedback_done IS NULL AND sb.userId=:userId
                ORDER BY $sidx $sord
                LIMIT :st, :lt"
            )
            ->bindValue(":userId", $userId)
            ->bindValue(":st", $start)
            ->bindValue(":lt", $limit)
            ->queryAll();

        $monthArray = array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'Mar',
            6 => 'June',
            7 => 'July',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec',
        );
        $coursetype = array(
            1 => 'Core',
            2 => 'Elective',
            3 => 'Skilled',
        );

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];

            $tutors_list = Yii::$app->db->createCommand(
                "SELECT GROUP_CONCAT(u.email SEPARATOR ', ')
                FROM adm_batch_current_courses_tutor AS abcct
                JOIN user AS u ON u.id=abcct.teacher_id
                WHERE abcct.current_course_id=:current_course_id
                GROUP BY abcct.current_course_id"
            )
            ->bindValue(":current_course_id", $row['id'])
            ->queryScalar();

            $responce->rows[$i]['cell'] = array(
                $row['course_title'], $row['course_code'],  $row['semester'], $coursetype[$row['courses_type']], $row['course_credit'],
                $row['year_start'], $monthArray[$row['month_start']], $row['year_end'], $monthArray[$row['month_end']], $tutors_list,
                $row['username'], $row['email'],
                $row['batch_id'], $row['program_id'], $row['dept_id'], $row['school_id'], $row['teacher_id'], $row['group_id']

            );
            $i++;
        }

        return json_encode($responce);
    }
    public function actionSave_batch_courses_feedback_now()
    {
        $StudentFeedbackData = new StudentFeedbackData();
        $userId = Yii::$app->user->id;
        $student_id = Yii::$app->db->createCommand("SELECT id FROM student_basic WHERE userId=:userId")->bindValue(":userId", $userId)->queryScalar();

        if ($StudentFeedbackData->load(Yii::$app->request->post()) && $StudentFeedbackData->validate()) {
            if($StudentFeedbackData->save()) {
                \Yii::$app->db->createCommand("UPDATE student_courses SET feedback_done=123 WHERE student_id=:student_id AND batch_course_id=:batch_course_id")
                ->bindParam(':student_id', $student_id)
                ->bindValue(':batch_course_id', $StudentFeedbackData->student_course_id)
                ->execute();

                return "insert";
            }
            else return "insert-error";
        }
        else {
            if($StudentFeedbackData->errors) return "error-validation";
            else return json_encode("unauthorized-access");
        }
    }    
    public function actionGet_batch_courses_feedback()
    {
        $userId = Yii::$app->user->identity->id;

        $limit = $_POST['rows'] *1;
        $page = $_POST['page'] *1;
        $sidx = HtmlPurifier::process($_POST['sidx']);
        $sord = HtmlPurifier::process($_POST['sord']);

        if(!$sidx) $sidx = "course_code";
        if(!$sord) $sord = "ASC";

        // $batch_id = $_POST['batch_id'] *1;
        $count = Yii::$app->db->createCommand(
            "SELECT
                COUNT(*)
            FROM
                adm_batch_current_courses AS abcc
            LEFT JOIN student_courses AS sc ON sc.batch_course_id=abcc.id
            LEFT JOIN student_basic AS sb ON sb.id=sc.student_id
            WHERE
                abcc.feedback IS NOT NULL AND sc.feedback_done IS NOT NULL AND sb.userId=:userId"
            )
            ->bindValue(":userId", $userId)
            ->queryScalar();

        if($count > 0) $total_pages = ceil($count/$limit);
        else $total_pages = 0;

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)

        if($start < 0) $start = 0;
        
        $i = 0;
        $responce = new \stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;

        //getting the data from mysql table and total record count
        $result = Yii::$app->db->createCommand(
                "SELECT abcc.*, u.username, u.email
                FROM adm_batch_current_courses AS abcc
                LEFT JOIN user AS u ON u.id=abcc.teacher_id
                LEFT JOIN student_courses AS sc ON sc.batch_course_id=abcc.id
                LEFT JOIN student_basic AS sb ON sb.id=sc.student_id
                WHERE abcc.feedback IS NOT NULL AND sc.feedback_done IS NOT NULL AND sb.userId=:userId
                ORDER BY $sidx $sord
                LIMIT :st, :lt"
            )
            ->bindValue(":userId", $userId)
            ->bindValue(":st", $start)
            ->bindValue(":lt", $limit)
            ->queryAll();

        $monthArray = array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'Mar',
            6 => 'June',
            7 => 'July',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec',
        );
        $coursetype = array(
            1 => 'Core',
            2 => 'Elective',
            3 => 'Skilled',
        );

        foreach($result as $row) {
            $responce->rows[$i]['id']= $row['id'];

            $tutors_list = Yii::$app->db->createCommand(
                "SELECT GROUP_CONCAT(u.email SEPARATOR ', ')
                FROM adm_batch_current_courses_tutor AS abcct
                JOIN user AS u ON u.id=abcct.teacher_id
                WHERE abcct.current_course_id=:current_course_id
                GROUP BY abcct.current_course_id"
            )
            ->bindValue(":current_course_id", $row['id'])
            ->queryScalar();

            $responce->rows[$i]['cell'] = array(
                $row['course_title'], $row['course_code'],  $row['semester'], $coursetype[$row['courses_type']], $row['course_credit'],
                $row['year_start'], $monthArray[$row['month_start']], $row['year_end'], $monthArray[$row['month_end']], $tutors_list,
                $row['username'], $row['email'],
                $row['batch_id'], $row['program_id'], $row['dept_id'], $row['school_id'], $row['teacher_id'], $row['group_id']

            );
            $i++;
        }

        return json_encode($responce);
    }    
}
