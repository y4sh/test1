<?php

namespace app\modules\course\models;

use Yii;

class StudentFeedbackData extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'student_feedback_data';
    }

    public function rules()
    {
        return [
            [['student_course_id', 'student_id', 'user_id', 'subjectmatter_knowledge', 'lecture_preparedness', 'abilityto_inspire', 'coursematerial_selection', 'presentationand_teachingstyle', 'abilitytodraw_interest', 'availableto_help', 'classtime_utilisation', 'assignmentsand_tests', 'evaluationand_grading', 'test_quality', 'reading_relavance', 'complementrystudy_materials', 'learningand_understanding', 'course_rating', 'self_efforts', 'expected_grade', 'suggested_improvements'], 'required'],
            [['student_course_id', 'student_id', 'user_id', 'subjectmatter_knowledge', 'lecture_preparedness', 'abilityto_inspire', 'coursematerial_selection', 'presentationand_teachingstyle', 'abilitytodraw_interest', 'availableto_help', 'classtime_utilisation', 'assignmentsand_tests', 'evaluationand_grading', 'test_quality', 'reading_relavance', 'complementrystudy_materials', 'learningand_understanding', 'course_rating'], 'integer'],
            [['self_efforts'], 'string', 'max' => 10],
            [['expected_grade'], 'string', 'max' => 2],
            [['suggested_improvements'], 'string', 'max' => 250],
        ];
    }

    public function attributeLabels()
    {
        return [
          /*  'id' => 'ID',
            'student_course_id' => 'Student Course ID',
            'subjectmatter_knowledge' => 'Subjectmatter Knowledge',
            'lecture_preparedness' => 'Lecture Preparedness',
            'abilityto_inspire' => 'Abilityto Inspire',
            'coursematerial_selection' => 'Coursematerial Selection',
            'presentationand_teachingstyle' => 'Presentationand Teachingstyle',
            'abilitytodraw_interest' => 'Abilitytodraw Interest',
            'availableto_help' => 'Availableto Help',
            'classtime_utilisation' => 'Classtime Utilisation',
            'assignmentsand_tests' => 'Assignmentsand Tests',
            'evaluationand_grading' => 'Evaluationand Grading',
            'test_quality' => 'Test Quality',
            'reading_relavance' => 'Reading Relavance',
            'complementrystudy_materials' => 'Complementrystudy Materials',
            'learningand_understanding' => 'Learningand Understanding',
            'course_rating' => 'Course Rating',
            'self_efforts' => 'Self Efforts',
            'expected_grade' => 'Expected Grade',
            'suggested_improvements' => 'Suggested Improvements',*/
            "subjectmatter_knowledge" => "Knowledge Of the subject Matter.",
            "lecture_preparedness" => "Prepraredness for teaching in class.",
            'abilityto_inspire' => "Ability  and efforts to inspire and motivate students.",
            'coursematerial_selection' => "Selection of course material.",
            'presentationand_teachingstyle' => "Effectiveness of presentation and teaching style.",
            "abilitytodraw_interest" => "Ability to increase your interest in the subject.",
            "availableto_help" => "Availability to help students after class",
            "classtime_utilisation" => "Use of class time",
            "assignmentsand_tests" => "Numbers of tests and assignments given.",
            "evaluationand_grading" => "Evaluation and grading system.",
            "test_quality" => "Quality of tests/assignment",
            "reading_relavance" => "Relevance of Readings.",
            "complementrystudy_materials" => "Texts and readings complemented class room lectures.",
            "learningand_understanding" => "Learning and understanding of the subject by yourself",
            "course_rating" => "Overall rating of this course",
            "self_efforts" => "How Much effort did you put into this course?Check any on",
            "expected_grade" => "What grade do you expect to get in this course?",
            "suggested_improvements" => "Your Suggestion to improve this course"            
        ];
    }
}
